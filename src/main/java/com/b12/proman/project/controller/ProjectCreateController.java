//package com.b12.proman.project.controller;
//
//import com.b12.proman.project.service.ProjectServiceImpl;
//import com.jagrosh.jdautilities.command.Command;
//import com.jagrosh.jdautilities.command.CommandEvent;
//import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
//import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.concurrent.TimeUnit;
//
//@Service
//public class ProjectCreateController extends Command {
//
//    @Autowired
//    private final EventWaiter waiter;
//
//    public ProjectCreateController(@Autowired EventWaiter waiter){
//        this.waiter = waiter;
//        this.name = "Name di ProjectCreateController";
//        this.aliases = new String[]{"Aliases di ProjectCreateController"};
//        this.help = "Help di ProjectCreateController";
//    }
//
//    ProjectServiceImpl projectService = new ProjectServiceImpl();
//
//    @Override
//    public void execute(CommandEvent event) {
////        Tes judul proyek
//        event.reply("Input the project title");
////        Wait for response
//        createProjectTitle(event);
//
////        Tes deskripsi proyek
//        event.reply("Input the project description");
//
//        createProjectDescription(event);
//
////        Tes member proyek
//        event.reply("Input the project member");
//
//        createProjectMember(event);
//
////        Tes deadline proyek
//        event.reply("Input the project deadline");
//
//        createProjectDeadline(event);
//    }
//
//    protected void createProjectTitle(CommandEvent event){
//        waiter.waitForEvent(MessageReceivedEvent.class,
//                // make sure it's by the same user, and in the same channel, and for safety, a different message
//                e -> {
//                    return e.getAuthor().equals(event.getAuthor())
//                            && e.getChannel().equals(event.getChannel())
//                            && !e.getMessage().equals(event.getMessage());
//                },
//                // respond, inserting the name they listed into the response
//                e -> {
//                    event.reply("The project title is `" + e.getMessage().getContentRaw() + "` Created by `" + e.getJDA().getSelfUser().getName() + "`");
//                },
//                // if the user takes more than a minute, time out
//                1, TimeUnit.MINUTES, () -> event.reply("Sorry, you took too long."));
//    }
//
//    protected void createProjectDescription(CommandEvent event){
//        waiter.waitForEvent(MessageReceivedEvent.class,
//                // make sure it's by the same user, and in the same channel, and for safety, a different message
//                e -> {
//                    return e.getAuthor().equals(event.getAuthor())
//                            && e.getChannel().equals(event.getChannel())
//                            && !e.getMessage().equals(event.getMessage());
//                },
//                // respond, inserting the name they listed into the response
//                e -> {
//                    event.reply("The project description is `" + e.getMessage().getContentRaw() + "` Created by `" + e.getJDA().getSelfUser().getName() + "`");
//                },
//                // if the user takes more than a minute, time out
//                1, TimeUnit.MINUTES, () -> event.reply("Sorry, you took too long."));
//    }
//
//    protected void createProjectMember(CommandEvent event){
//        waiter.waitForEvent(MessageReceivedEvent.class,
//                // make sure it's by the same user, and in the same channel, and for safety, a different message
//                e -> {
//                    return e.getAuthor().equals(event.getAuthor())
//                            && e.getChannel().equals(event.getChannel())
//                            && !e.getMessage().equals(event.getMessage());
//                },
//                // respond, inserting the name they listed into the response
//                e -> {
//                    event.reply("The project member is `" + e.getMessage().getContentRaw() + "` Created by `" + e.getJDA().getSelfUser().getName() + "`");
//                },
//                // if the user takes more than a minute, time out
//                1, TimeUnit.MINUTES, () -> event.reply("Sorry, you took too long."));
//    }
//
//    protected void createProjectDeadline(CommandEvent event){
//        waiter.waitForEvent(MessageReceivedEvent.class,
//                // make sure it's by the same user, and in the same channel, and for safety, a different message
//                e -> {
//                    return e.getAuthor().equals(event.getAuthor())
//                            && e.getChannel().equals(event.getChannel())
//                            && !e.getMessage().equals(event.getMessage());
//                },
//                // respond, inserting the name they listed into the response
//                e -> {
//                    event.reply("The project deadline is `" + e.getMessage().getContentRaw() + "` Created by `" + e.getJDA().getSelfUser().getName() + "`");
//                },
//                // if the user takes more than a minute, time out
//                1, TimeUnit.MINUTES, () -> event.reply("Sorry, you took too long."));
//    }
//}
