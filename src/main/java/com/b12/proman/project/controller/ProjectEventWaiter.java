package com.b12.proman.project.controller;

import com.b12.proman.project.service.ProjectServiceImpl;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;

public class ProjectEventWaiter extends Command {

    ProjectServiceImpl projectService;
    User author;

    public ProjectEventWaiter(ProjectServiceImpl projectService) {
        this.name = "Project Event Waiter";
        this.aliases = new String[]{"Project"};
        this.help = "Create or Edit Project";
        this.botPermissions = new Permission[]{Permission.MESSAGE_EMBED_LINKS};
        this.guildOnly = false;
        this.projectService = projectService;
    }

    @Override
    protected void execute(CommandEvent event) {
        author = event.getAuthor();

        String[] args = event.getArgs().split("\\s+");

        if (event.getArgs().isEmpty()) {
            invalidInput(event);
        }

        if (args[0] == "/ngetest ") {
            if (args[1] == "-createeeee") {
                createProject(event);
                event.reactSuccess();
            } else if (args[1] == "-edittttt") {
                editProject(event);
                event.reactSuccess();
            }
        }

    }

    public void createProject(CommandEvent event) {
        projectService.createProject();
        event.reply((Message) new EmbedBuilder()
            .setTitle("Create a project")
            .setDescription("Project creation success!")
            .setFooter("Called by: " + author, event.getAuthor().getAvatarUrl()));
    }

    public void editProject(CommandEvent event) {
        String[] args = event.getArgs().split("\\s+");
        projectService.editProject(args[2]);
        event.reply((Message) new EmbedBuilder()
            .setTitle("Edit a project")
            .setDescription("Project editing success!")
            .setFooter("Called by: " + author, event.getAuthor().getAvatarUrl()));
    }

    public void invalidInput(CommandEvent event) {
        event.reply((Message) new EmbedBuilder()
            .setTitle("Invalid Input")
            .setDescription("Type '/help' to see the command list")
            .setFooter("Called by: " + author, event.getAuthor().getAvatarUrl())
            .build());
        event.reactError();
    }




}
