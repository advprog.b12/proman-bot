package com.b12.proman.project.controller;

import com.b12.proman.history.service.HistoryService;
import com.b12.proman.project.core.Project;
import com.b12.proman.project.core.Task;
import com.b12.proman.project.repository.ProjectRepository;
import com.b12.proman.project.service.ProjectService;
import com.b12.proman.project.service.TaskService;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectController {

    @Autowired
    HistoryService historyService;

    @Autowired
    ProjectService projectService;

    @Autowired
    TaskService taskService;

    public void onGuildMessageReceivedProject(GuildMessageReceivedEvent event) {

        String[] args = event.getMessage().getContentRaw().split("\\s+");
        String author = event.getMember().getEffectiveName();
        // CommandEvent commandEvent = event;

        EmbedBuilder projectEmbed = new EmbedBuilder();
        Project project;

        ProjectRepository projectRepository;

        if (args.length == 1) {

            projectEmbed.setTitle("List of Project command");
            projectEmbed.setDescription(" '/project-create' for create a new project" + "\n"
                    + "'/project-list' to see the list of existing project" + "\n"
                    + "'/project-get-<project name>' to get a specific project" + "\n"
                    + "'/project-edit-<project name>' for editing a project" + "\n"
                    // + "'/project-progress-<project name>' for seeing the progress of a project" +
                    // "\n"
                    + "'/project-delete-<project name>' for deleting a project");

            projectEmbed.setColor(0x0004ff);
            projectEmbed.setFooter("Called by " + author, event.getAuthor().getAvatarUrl());

            event.getChannel().sendTyping().queue();
            event.getChannel().sendMessage(projectEmbed.build()).queue();
        } else {
            switch (args[1]) {
                // case ("-createe"):
                // projectCreateController.execute();
                // break;
                case ("-create"):
                    int counter = 2;
                    String cmd = "";
                    String prTitle = "";
                    String prDesc = "";
                    String prMember = "";
                    String prDeadline = "";

                    while (counter < args.length) {
                        if (args[counter].equals("{title}")) {
                            cmd = "{title}";
                        } else if (args[counter].equals("{description}")) {
                            cmd = "{description}";
                        } else if (args[counter].equals("{member}")) {
                            cmd = "{member}";
                        } else if (args[counter].equals("{deadline}")) {
                            cmd = "{deadline}";
                        } else {
                            if (cmd.equals("{title}")) {
                                prTitle += args[counter] + " ";
                            } else if (cmd.equals("{description}")) {
                                prDesc += args[counter] + " ";
                            } else if (cmd.equals("{member}")) {
                                prMember += args[counter] + " ";
                            } else if (cmd.equals("{deadline}")) {
                                prDeadline += args[counter] + " ";
                            }
                        }
                        counter++;
                    }

                    prTitle = prTitle.substring(0, prTitle.length() - 1);
                    System.out.println(prTitle);
                    System.out.println(prDesc);
                    System.out.println(prMember);
                    System.out.println(prDeadline);

                    projectEmbed.setTitle("Project Create");
                    projectEmbed.setDescription("The project has been successfully created");

                    Project newProject = projectService.newProject(prTitle, prDesc, prMember, prDeadline);

                    String projectName1 = newProject.getTitle();
                    String desc1 = "Created a new project titled: " + projectName1;
                    historyService.addHistory(author, projectName1, desc1);

                    break;
                case ("-edit"):
                    int counter2 = 2;
                    String cmd2 = "";
                    String prTitle2 = "";
                    String prDesc2 = "";
                    String prMember2 = "";
                    String prDeadline2 = "";
                    String oldTitle = "";

                    while (cmd2.equals("")) {
                        if (args[counter2].equals("{title}")) {
                            cmd2 = "{title}";
                        } else if (args[counter2].equals("{description}")) {
                            cmd2 = "{description}";
                        } else if (args[counter2].equals("{member}")) {
                            cmd2 = "{member}";
                        } else if (args[counter2].equals("{deadline}")) {
                            cmd2 = "{deadline}";
                        } else {
                            oldTitle += args[counter2] + " ";
                        }
                        counter2++;
                    }

                    oldTitle = oldTitle.substring(0, oldTitle.length() - 1);

                    while (counter2 < args.length) {
                        if (cmd2.equals("{title}")) {
                            prTitle2 += args[counter2] + " ";
                        } else if (cmd2.equals("{description}")) {
                            prDesc2 += args[counter2] + " ";
                        } else if (cmd2.equals("{member}")) {
                            prMember2 += args[counter2] + " ";
                        } else if (cmd2.equals("{deadline}")) {
                            prDeadline2 += args[counter2] + " ";
                        }
                        counter2++;
                    }

                    System.out.println(prTitle2);
                    System.out.println(prDesc2);
                    System.out.println(prMember2);
                    System.out.println(prDeadline2);

                    if (cmd2.equals("{title}")) {
                        prTitle2 = prTitle2.substring(0, prTitle2.length() - 1);
                        projectService.editTitle(oldTitle, prTitle2);
                    } else if (cmd2.equals("{description}")) {
                        prTitle2 = oldTitle;
                        projectService.editDescription(oldTitle, prDesc2);
                    } else if (cmd2.equals("{member}")) {
                        prTitle2 = oldTitle;
                        projectService.editMember(oldTitle, prMember2);
                    } else if (cmd2.equals("{deadline}")) {
                        prTitle2 = oldTitle;
                        projectService.editDeadline(oldTitle, prDeadline2);
                    }

                    projectEmbed.setTitle("Project Edit");
                    // project = projectService.editProject(prTitle2);
                    project = projectService.getProject(prTitle2);
                    if (project != null) {
                        projectEmbed.setDescription(project.getTitle() + " has been successfully edited");

                        String projectName2 = project.getTitle();
                        String desc2 = "Made some changes in the project titled: " + projectName2;
                        historyService.addHistory(author, projectName2, desc2);
                    } else {
                        projectEmbed.setDescription("The project didn't exist, try again");
                    }

                    break;
                case ("-delete"):
                    String[] titlesDel = Arrays.copyOfRange(args, 2, args.length);
                    String titleDel = String.join(" ", titlesDel);
                    projectEmbed.setTitle("Project Delete");
                    projectEmbed.setDescription("Delete the project");
                    projectService.deleteProject(titleDel);

                    String desc3 = "Deleted the project titled: " + titleDel;
                    historyService.addHistory(author, titleDel, desc3);

                    break;
                case ("-get"):
                    String[] titlesGet = Arrays.copyOfRange(args, 2, args.length);
                    String titleGet = String.join(" ", titlesGet);
                    projectEmbed.setTitle("Get a Project");
                    project = projectService.getProject(titleGet);
                    projectEmbed.addField(project.getTitle(), "\nDescription: " + project.getDescription()
                            + "\nMember: " + project.getMember() + "\nDeadline: " + project.getDeadline(), false);
                    break;
                case ("-list"):
                    projectEmbed.setTitle("Project List");
                    projectEmbed.setDescription("List of all project in this server");

                    List<Project> projects = projectService.getAllProject();

                    for (Project p : projects) {
                        projectEmbed.addField(p.getTitle(), "\nDescription: " + p.getDescription() + "\nMember: "
                                + p.getMember() + "\nDeadline: " + p.getDeadline(), false);
                    }

                    break;
                case ("-assign"):
                    String memberId = event.getAuthor().getId();
                    taskService.assignTask(args[2], args[3], args[4], memberId);
                    projectEmbed.setTitle("Task Assigned");
                    projectEmbed.setDescription("Assign a task for a project");

                    String desc4 = "Assigned a new task titled: {" + args[2] + "} for " + args[4];
                    historyService.addHistory(author, args[2], desc4);

                    break;
                case ("-ambil"):
                    Task task = taskService.getTask(args[2]);
                    if (task == null) {
                        projectEmbed.setTitle("Task not found");
                        projectEmbed.setDescription(
                                "Task tidak ditemukan, Coba untuk melakukan perintah /task -assign yyyy-MM-dd-HH-mm-ss");

                    } else {
                        String judul = task.getTitle();
                        String deskripsi = task.getDeskripsi();
                        String deadline = task.getDeadline().format(DateTimeFormatter.ISO_DATE_TIME);

                        projectEmbed.setTitle("Get a Task");
                        projectEmbed.setDescription("judul : " + judul + "\n" + "deskripsi : " + deskripsi + "\n"
                                + "deadline : " + deadline + "\n");
                    }

                    break;

                default:
                    projectEmbed.setTitle("Invalid Input");
                    projectEmbed.setDescription("Type '/help' to see the list of all commands");

                    break;
            }

            event.getChannel().sendTyping().queue();
            event.getChannel().sendMessage(projectEmbed.build()).queue();

        }
    }

}
