package com.b12.proman.project.repository;

import com.b12.proman.project.core.Task;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Repository;

@Repository
public class TaskRepositoryImpl implements TaskRepository  {

    private Map<String, Task> taskBanyak = new HashMap<>();

    public Task getTask(String title) {
        return taskBanyak.get(title);
    }

    public Map<String, Task> getTasks() {
        return taskBanyak;
    }

    public Task save(Task savedTask) {
        Task existingTask = taskBanyak.get(savedTask.getTitle());
        if (existingTask == null) {
            taskBanyak.put(savedTask.getTitle(), savedTask);
            return savedTask;
        } else {
            return null;
        }
    }
}