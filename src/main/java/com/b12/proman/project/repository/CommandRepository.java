package com.b12.proman.project.repository;

import com.b12.proman.project.core.Project;
import com.b12.proman.project.core.Task;
import com.b12.proman.project.core.command.Command;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;


@Repository
public class CommandRepository {

    private Map<String, Command> commands = new HashMap<>();

    public void newCommand(Command command) {
        commands.put(command.nameCommand(), command);
    }

    public Project executeCreate(Project project) {
        return commands.get("create").createProject(project);
    }

    public Project executeEdit(Project project) {
        return commands.get("edit").editProject(project);
    }

    public Project executeDelete(String nameProject) {
        //return commands.get("delete").deleteProject(nameProject);
        return null;
    }

    public Project executeGet(String nameProject) {
        return commands.get("get").getProject(nameProject);
    }

    public List<Project> executeListProject() {
        return commands.get("projects").listProject();
    }

    public Task executeAssign(String[] list, String memberId) {
        return commands.get("assign").assignTask(list, memberId);
    }

    public Task[] executeListTask() {
        return commands.get("tasks").listTask();
    }

}
