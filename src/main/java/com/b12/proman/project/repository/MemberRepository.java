package com.b12.proman.project.repository;

import com.b12.proman.project.core.Member;

public interface MemberRepository {

    Member getMemberById(String memberId);

    void addMember(Member member);
}
