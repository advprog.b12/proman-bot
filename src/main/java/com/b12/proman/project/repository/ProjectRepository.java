package com.b12.proman.project.repository;

import com.b12.proman.project.core.Project;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

@Repository
public class ProjectRepository {
    private Map<String, Project> projectList = new HashMap<>();

    public void addProject(Project project) {
        String projectTitle = project.getTitle();
        projectList.put(projectTitle, project);
    }

    public Project getProjectByTitle(String projectTitle) {
        return projectList.get(projectTitle);
    }

    public List<Project> getProjectList() {
        return new ArrayList<>(projectList.values());
    }

    public void deleteProject(Project project) {
        String projectTitle = project.getTitle();
        projectList.remove(projectTitle, project);
    }

    public void deleteProjectByTitle(String projectTitle) {
        projectList.remove(projectTitle);
    }

}
