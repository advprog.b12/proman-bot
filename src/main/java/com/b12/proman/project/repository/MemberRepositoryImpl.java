package com.b12.proman.project.repository;

import com.b12.proman.PromanApplication;
import com.b12.proman.project.core.Member;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.User;
import org.springframework.stereotype.Repository;

@Repository
public class MemberRepositoryImpl implements MemberRepository {

    private List<Member> memberList = new ArrayList<>();

    public Member getMemberById(String memberId) {
        for (Member member: memberList) {
            if (member.getId().equals(memberId)) {
                return member;
            }
        }
        // if the member has not been created
        Member member = new Member(memberId);
        addMember(member);
        return member;
    }

    public void addMember(Member member) {
        JDA jda = PromanApplication.getJDA();
        User user = jda.getUserById(member.getId());
        member.setUser(user);
        memberList.add(member);
    }
}
