package com.b12.proman.project.repository;

import com.b12.proman.project.core.Task;
import java.util.Map;

public interface TaskRepository {

    Map<String, Task> getTasks();

    Task getTask(String title);

    Task save(Task savedTask);
}
