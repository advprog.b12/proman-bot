package com.b12.proman.project.service;

import com.b12.proman.project.core.Project;
import com.b12.proman.project.core.Task;
import com.b12.proman.project.core.command.AssignTaskCommand;
import com.b12.proman.project.core.command.CreateProjectCommand;
import com.b12.proman.project.core.command.EditProjectCommand;
import com.b12.proman.project.core.command.ListTaskCommand;
import com.b12.proman.project.repository.CommandRepository;
import com.b12.proman.project.repository.ProjectRepository;
import com.jagrosh.jdautilities.command.CommandEvent;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    CommandRepository commandRepository;

    @Autowired
    ProjectRepository projectRepository;

    EditProjectCommand editProjectCommand = new EditProjectCommand();

    public CommandEvent commandEvent;

    @Override
    public Project createProject() {
        commandRepository.newCommand(new CreateProjectCommand());
        Project projectCreated = new Project();
        Project project = commandRepository.executeCreate(projectCreated);
        projectRepository.addProject(project);
        return project;
    }

    @Override
    public Project newProject(String title, String desc, String member, String deadline) {
        Project project = new Project();
        project.setTitle(title);
        project.setDescription(desc);
        project.setMember(member);
        project.setDeadline(deadline);

        projectRepository.addProject(project);
        return project;
    }

    @Override
    public Project editProject(String projectName) {
        //commandRepository.newCommand(new EditProjectCommand());
        //Project editedProject = commandRepository.executeEdit(projectName);
        Project editedProject = projectRepository.getProjectByTitle(projectName);
        String projectTitle = editedProject.getTitle();
        //String pDescription = editedProject.getDescription();
        //String pMember = editedProject.getMember();
        //String pDeadline = editedProject.getDeadline();

        Project newProject = editProjectCommand.editProject(editedProject);
        //String newTitle = newProject.getTitle();
        projectRepository.deleteProjectByTitle(projectTitle);
        projectRepository.addProject(newProject);

        return newProject;
    }

    @Override
    public Project editTitle(String oldTitle, String newTitle) {
        Project editedProject = projectRepository.getProjectByTitle(oldTitle);
        editedProject.setTitle(newTitle);

        Project newProject = editedProject;
        projectRepository.deleteProjectByTitle(oldTitle);
        projectRepository.addProject(newProject);

        return newProject;
    }

    @Override
    public Project editDescription(String oldTitle, String newDescription) {
        Project editedProject = projectRepository.getProjectByTitle(oldTitle);
        editedProject.setDescription(newDescription);
        return editedProject;
    }

    @Override
    public Project editMember(String oldTitle, String newMember) {
        Project editedProject = projectRepository.getProjectByTitle(oldTitle);
        editedProject.setMember(newMember);
        return editedProject;
    }

    @Override
    public Project editDeadline(String oldTitle, String newDeadline) {
        Project editedProject = projectRepository.getProjectByTitle(oldTitle);
        editedProject.setDeadline(newDeadline);
        return editedProject;
    }

    @Override
    public void deleteProject(String nameProject) {
        projectRepository.deleteProjectByTitle(nameProject);
    }

    @Override
    public Project getProject(String arg) {
        return projectRepository.getProjectByTitle(arg);
    }

    @Override
    public List<Project> getAllProject() {
        return projectRepository.getProjectList();
    }

    @Override
    public String assignTask() {
        commandRepository.newCommand(new AssignTaskCommand());
        Task task = commandRepository.executeAssign(null, null);
        return "Task ... berhasil diassign kepada ...";
    }

    @Override
    public String listTask() {
        commandRepository.newCommand(new ListTaskCommand());
        Task[] task = commandRepository.executeListTask();
        return "...";
    }
}
