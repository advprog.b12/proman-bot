package com.b12.proman.project.service;

import com.b12.proman.project.core.Member;
import com.b12.proman.project.core.Task;
import com.b12.proman.project.repository.MemberRepository;
import com.b12.proman.project.repository.TaskRepository;
import com.b12.proman.reminder.core.Reminder;
import com.b12.proman.reminder.repository.ReminderRepository;
import java.util.Map;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MemberServiceImpl implements MemberService {

    @Autowired
    MemberRepository memberRepository;

    @Autowired
    ReminderRepository reminderRepository;

    @Autowired
    TaskRepository taskRepository;

    @Override
    public boolean isLeader(String memberId, String nameProject) {
        Member member = memberRepository.getMemberById(memberId);
        Map<String, Boolean> projects = member.getProjects();
        return projects.get(nameProject);
    }

    @Override
    public void setLeader(String memberId, String nameProject) {
        Member member = memberRepository.getMemberById(memberId);
        Map<String, Boolean> projects = member.getProjects();
        projects.replace(nameProject, true);
        member.setProjects(projects);
    }

    @Override
    public void addProject(String memberId, String nameProject) {
        Member member = memberRepository.getMemberById(memberId);
        Map<String, Boolean> projects = member.getProjects();
        projects.put(nameProject, false);
        member.setProjects(projects);
    }

    @Override
    public void deleteProject(String memberId, String nameProject) {
        Member member = memberRepository.getMemberById(memberId);
        Map<String, Boolean> projects = member.getProjects();
        projects.remove(nameProject);
        member.setProjects(projects);
    }

    @Override
    public void addTask(String memberId, String nameTask) {
        Member member = memberRepository.getMemberById(memberId);
        Map<String, Reminder> tasks = member.getTasks();
        Reminder reminder = new Reminder(nameTask, member.getUser());
        tasks.put(nameTask, reminder);
        Task task = taskRepository.getTask(nameTask);
        ///task.link(member.getMemberInformer());
        reminderRepository.add(reminder);
        member.setTasks(tasks);
    }

    @Override
    public void update(String memberId, String projectName, String detail) {

        if (detail.equals("delete")) {
            deleteProject(memberId, projectName);
        } else if (detail.equals("create")) {
            addProject(memberId, projectName);
        }
        Member member = memberRepository.getMemberById(memberId);
        MessageEmbed embeddedMessage = createEmbedMessage(projectName, detail).build();
        User user = member.getUser();
        user.openPrivateChannel()
            .flatMap(channel -> channel.sendMessage(embeddedMessage))
            .queue();
    }

    @Override
    public EmbedBuilder createEmbedMessage(String projectName, String detail) {

        EmbedBuilder builder = new EmbedBuilder();
        if (detail.equals("edit")) {
            builder.setTitle("Project \"" + projectName + "\" has been edited!");
        } else if (detail.equals("delete")) {
            builder.setTitle("Project \"" + projectName + "\" has been deleted!");
        } else {
            builder.setTitle("You are added to project \"" + projectName + "\"!");
        }

        builder.setDescription("Contact your leader for further information.");
        builder.setColor(0xff0000);
        return builder;
    }

}
