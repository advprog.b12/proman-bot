package com.b12.proman.project.service;

import com.b12.proman.project.core.Task;

public interface TaskService {

    String namaMember(String member);

    String namaTask(String taskName);

    String deadline(String waktu);

    void assignTask(String judul, String deskripsi, String deadline, String memberId);

    Task getTask(String judul);
}