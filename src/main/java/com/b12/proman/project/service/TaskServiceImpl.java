package com.b12.proman.project.service;

import com.b12.proman.project.core.Task;
import com.b12.proman.project.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    TaskRepository taskRepository;

    @Autowired
    MemberService memberService;

    @Override
    public String namaMember(String member) {
        return "Hello, " + member + "!"
                + " Anda masih mempunyai waktu 24 jam untuk mengerjakan task dengan detail berukut:";
    }

    @Override
    public String namaTask(String task) {
        return "Project anda: \n " + task;
    }

    @Override
    public String deadline(String waktu) {
        return "Deadline: \n " + waktu;
    }

    public void assignTask(String judul, String deskripsi, String deadline, String memberId) {
        memberService.addTask(memberId, judul);
        Task task = new Task(judul, deskripsi, deadline);
        taskRepository.save(task);
    }

    public Task getTask(String judul) {
        return taskRepository.getTask(judul);
    }

}
