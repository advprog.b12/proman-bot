package com.b12.proman.project.service;

import net.dv8tion.jda.api.EmbedBuilder;

public interface MemberService {

    boolean isLeader(String memberId, String nameProject);

    void setLeader(String memberId, String nameProject);

    void addProject(String memberId, String nameProject);

    void deleteProject(String memberId, String nameProject);

    void addTask(String memberId, String nameTask);

    void update(String memberId, String projectName, String detail);

    EmbedBuilder createEmbedMessage(String projectName, String detail);

}
