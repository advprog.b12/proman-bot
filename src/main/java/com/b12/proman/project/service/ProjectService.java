package com.b12.proman.project.service;

import com.b12.proman.project.core.Project;
import java.util.List;

public interface ProjectService {
    
    Project createProject();

    Project newProject(String title, String desc, String member, String deadline);

    Project editProject(String projectName);

    Project editTitle(String oldTitle, String newTitle);

    Project editDescription(String oldTitle, String newDescription);

    Project editMember(String oldTitle, String newMember);

    Project editDeadline(String oldTitle, String newDeadline);

    void deleteProject(String nameProject);

    Project getProject(String arg);

    List<Project> getAllProject();

    String assignTask();

    String listTask();

}
