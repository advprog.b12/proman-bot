package com.b12.proman.project.core;

import com.b12.proman.project.core.informer.MemberInformer;
import com.b12.proman.project.core.observer.NotifyTask;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class Task {

    private String title;
    private String memberId;
    private String deskripsi;
    private LocalDateTime deadline;
    private MemberInformer memberInformer;

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return this.title;
    }

    public Task(String j, String d, String ded) {
        this.title = j;
        this.deskripsi = d;
        this.deadline = LocalDateTime.parse(ded, DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss"));
    }

    public String getDeskripsi() {
        return deskripsi;
    }
    // buat ngetotify si member

    public LocalDateTime getDeadline() {
        return deadline;
    }

    public void notifyDate() {
        LocalDateTime waktuDeadline = getDeadline();
        Date waktuDed = Date.from(waktuDeadline.atZone(ZoneId.systemDefault()).toInstant());

        new Timer().schedule(new NotifyTask(memberId, title), waktuDed);
    }

    public void tellInformer(MemberInformer memberInformer) {
        memberInformer.informMember(this);
    }

    public void tellInformerAtDeadline() {
        var timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                tellInformer(memberInformer);
            }
        };

        timer.schedule(timerTask, convertToDate(deadline));
    }

    Date convertToDate(LocalDateTime dateToConvert) {
        return java.util.Date.from(dateToConvert.atZone(ZoneId.systemDefault()).toInstant());
    }

    public void link(MemberInformer memberInformer) {
        this.memberInformer = memberInformer;
    }
}
