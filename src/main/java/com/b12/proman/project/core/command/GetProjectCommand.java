package com.b12.proman.project.core.command;

import com.b12.proman.project.core.Project;
import com.b12.proman.project.core.Task;
import com.b12.proman.project.repository.ProjectRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

public class GetProjectCommand implements Command {

    @Autowired
    ProjectRepository projectRepository;

    @Override
    public Project createProject(Project project) {
        // do nothing
        return null;
    }

    @Override
    public Project editProject(Project project) {
        //do nothing
        return null;
    }

    @Override
    public void deleteProject(String nameProject) {
        // do nothing

    }

    @Override
    public Project getProject(String nameProject) {
        //TODO
        return projectRepository.getProjectByTitle(nameProject);
    }

    @Override
    public List<Project> listProject() {
        // do nothing
        return null;
    }

    @Override
    public Task assignTask(String[] list, String memberId) {
        // do nothing
        return null;
    }

    @Override
    public Task[] listTask() {
        // do nothing
        return null;
    }

    @Override
    public String nameCommand() {
        return "get";
    }
}
