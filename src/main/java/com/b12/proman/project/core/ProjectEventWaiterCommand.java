//package com.b12.proman.project.core;
//
//import com.jagrosh.jdautilities.command.Command;
//import com.jagrosh.jdautilities.command.CommandEvent;
//import com.jagrosh.jdautilities.command.annotation.JDACommand;
//import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
//import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
//import org.springframework.context.annotation.Bean;
//import org.springframework.stereotype.Component;
//
//import java.util.concurrent.TimeUnit;
//
//@Component
//public class ProjectEventWaiterCommand extends Command {
//    private final EventWaiter waiter;
//    private Project project;
//
//    public ProjectEventWaiterCommand(EventWaiter waiter) {
//        this.waiter = waiter;
//    }
//
//    @Override
//    protected void execute(CommandEvent event) {
//        createProjectTitleWaiter(event);
//        createProjectDescriptionWaiter(event);
//        createProjectMemberWaiter(event);
//        createProjectDeadlineWaiter(event);
//    }
//
//    public void createProjectTitleWaiter(CommandEvent commandEvent) {
//        commandEvent.reply("Set the project title: ");
//        waiter.waitForEvent(GuildMessageReceivedEvent.class, e -> e.getAuthor().equals(commandEvent.getAuthor()) && e.getChannel().equals(commandEvent.getChannel()), e -> {
//            //First argument is the event we are waiting for. Second is the condition that is checked when the event is triggered. Third is the code that will be run with the event and condition is fulfilled.
//            project.setTitle(e.getMessage().toString());
//        }, 30, TimeUnit.SECONDS, () -> commandEvent.reply("You did not give me a name to search. Try again.")); //Last 3 arguments specify how long to wait for the event and what to do if they never respond
//    }
//
//    public void createProjectDescriptionWaiter(CommandEvent commandEvent) {
//        commandEvent.reply("Set the project description: ");
//        waiter.waitForEvent(GuildMessageReceivedEvent.class, e -> e.getAuthor().equals(commandEvent.getAuthor()) && e.getChannel().equals(commandEvent.getChannel()), e -> {
//            //First argument is the event we are waiting for. Second is the condition that is checked when the event is triggered. Third is the code that will be run with the event and condition is fulfilled.
//            project.setDescription(e.getMessage().toString());
//        }, 30, TimeUnit.SECONDS, () -> commandEvent.reply("You did not give me a name to search. Try again.")); //Last 3 arguments specify how long to wait for the event and what to do if they never respond
//
//    }
//
//    public void createProjectMemberWaiter(CommandEvent commandEvent) {
//        commandEvent.reply("Select the project member: ");
//        waiter.waitForEvent(GuildMessageReceivedEvent.class, e -> e.getAuthor().equals(commandEvent.getAuthor()) && e.getChannel().equals(commandEvent.getChannel()), e -> {
//            //First argument is the event we are waiting for. Second is the condition that is checked when the event is triggered. Third is the code that will be run with the event and condition is fulfilled.
//            project.setMember(e.getMessage().toString());
//        }, 30, TimeUnit.SECONDS, () -> commandEvent.reply("You did not give me a name to search. Try again.")); //Last 3 arguments specify how long to wait for the event and what to do if they never respond
//
//    }
//
//    public void createProjectDeadlineWaiter(CommandEvent commandEvent) {
//        commandEvent.reply("Set the project deadline: ");
//        waiter.waitForEvent(GuildMessageReceivedEvent.class, e -> e.getAuthor().equals(commandEvent.getAuthor()) && e.getChannel().equals(commandEvent.getChannel()), e -> {
//            //First argument is the event we are waiting for. Second is the condition that is checked when the event is triggered. Third is the code that will be run with the event and condition is fulfilled.
//            project.setDeadline(e.getMessage().toString());
//        }, 30, TimeUnit.SECONDS, () -> commandEvent.reply("You did not give me a name to search. Try again.")); //Last 3 arguments specify how long to wait for the event and what to do if they never respond
//
//    }
//
//
//}
