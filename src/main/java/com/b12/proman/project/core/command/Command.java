package com.b12.proman.project.core.command;

import com.b12.proman.project.core.Project;
import com.b12.proman.project.core.Task;
import java.util.List;

public interface Command {
    Project createProject(Project project);

    Project editProject(Project project);

    void deleteProject(String nameProject);

    Project getProject(String nameProject);

    List<Project> listProject();

    Task assignTask(String[] list, String memberId);

    Task[] listTask();

    String nameCommand();
}
