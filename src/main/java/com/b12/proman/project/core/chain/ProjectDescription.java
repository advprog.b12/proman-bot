package com.b12.proman.project.core.chain;

import com.b12.proman.project.core.Project;
//import com.b12.proman.project.core.ProjectEventWaiterCommand;
import java.util.Scanner;

public class ProjectDescription extends CreateProjectChain {
    String output;
    Scanner input = new Scanner(System.in);
    //public ProjectEventWaiterCommand projectEventWaiterCommand;

    public ProjectDescription() {
        super();
    }

    @Override
    public Project creating(Project project) {
        if (project != null) {
            System.out.println("Set the project description: ");
            output = input.nextLine();
            project.setDescription(output);
            //projectEventWaiterCommand.createProjectDescriptionWaiter(commandEvent);
        }
        if (this.nextProcess != null) {
            this.nextProcess.creating(project);
        }
        return project;
    }
}
