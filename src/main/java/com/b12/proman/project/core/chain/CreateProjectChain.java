package com.b12.proman.project.core.chain;

import com.b12.proman.project.core.Project;

public abstract class CreateProjectChain {
    public CreateProjectChain nextProcess;

    public void setNextProcess(CreateProjectChain nextProcess) {
        this.nextProcess = nextProcess;
    }

    public abstract Project creating(Project project);
}
