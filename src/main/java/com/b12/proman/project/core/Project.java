package com.b12.proman.project.core;

public class Project {
    private String title;
    private String description;
    private String member;
    private String deadline;

    public Project(String title, String description, String member, String deadline) {
        this.title = title;
        this.description = description;
        this.member = member;
        this.deadline = deadline;
    }

    public Project() {
        super();
    }

    public Project(String projectTitle) {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMember() {
        return member;
    }

    public void setMember(String member) {
        this.member = member;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }
}

