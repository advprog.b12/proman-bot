package com.b12.proman.project.core.command;

import com.b12.proman.project.core.Project;
import com.b12.proman.project.core.Task;
import java.util.List;
import java.util.Scanner;

public class EditProjectCommand implements Command {
    //ProjectRepository projectRepository = new ProjectRepository();
    GetProjectCommand getProjectCommand = new GetProjectCommand();

    Scanner input = new Scanner(System.in);

    @Override
    public Project createProject(Project project) {
        // do nothing
        return null;
    }

    @Override
    public Project editProject(Project project) {
        //TODO
        if (project != null) {
            System.out.println("What attribute do you want to edit? ");
            String editAnswer = input.nextLine();

            switch (editAnswer) {
                case ("title"):
                    System.out.println("Set your new title: ");
                    String editTitle = input.nextLine();
                    project.setTitle(editTitle);
                    System.out.println("The title has been successfully edited with the new title " + project.getTitle());
                    break;
                case ("description"):
                    System.out.println("Set your new description: ");
                    String editDescription = input.nextLine();
                    project.setDescription(editDescription);
                    System.out.println("The description has been successfully edited with the new description " + project.getDescription());
                    break;
                case ("member"):
                    System.out.println("Set your new member: ");
                    String editMember = input.nextLine();
                    project.setMember(editMember);
                    System.out.println("The member has been successfully edited with the new member " + project.getMember());
                    break;
                case ("deadline"):
                    System.out.println("Set your new deadline: ");
                    String editDeadline = input.nextLine();
                    project.setDeadline(editDeadline);
                    System.out.println("The deadline has been successfully edited with the new deadline " + project.getDeadline());
                    break;
                default:
                    System.out.println("The input for editing should be 'title', 'description', 'member', and 'deadline' with case sensitive");
            }
        } else {
            System.out.println("The project doesn't exist (EDIT)");
        }

        return project;
    }

    @Override
    public void deleteProject(String nameProject) {
        // do nothing

    }

    @Override
    public Project getProject(String nameProject) {
        // do nothing
        return null;
    }

    @Override
    public List<Project> listProject() {
        // do nothing
        return null;
    }

    @Override
    public Task assignTask(String[] list, String memberId) {
        // do nothing
        return null;
    }

    @Override
    public Task[] listTask() {
        // do nothing
        return null;
    }

    @Override
    public String nameCommand() {
        return "edit";
    }
}
