package com.b12.proman.project.core.chain;

import com.b12.proman.project.core.Project;
//import com.b12.proman.project.core.ProjectEventWaiterCommand;
import java.util.Scanner;

public class ProjectMember extends CreateProjectChain {
    String output;
    //public ProjectEventWaiterCommand projectEventWaiterCommand;

    public ProjectMember() {
        super();
    }

    Scanner input = new Scanner(System.in);

    @Override
    public Project creating(Project project) {
        if (project != null) {
            System.out.println("Set project member: ");
            output = input.nextLine();
            project.setMember(output);
            //projectEventWaiterCommand.createProjectMemberWaiter(commandEvent);
        }
        if (this.nextProcess != null) {
            this.nextProcess.creating(project);
        }
        return project;
    }
}
