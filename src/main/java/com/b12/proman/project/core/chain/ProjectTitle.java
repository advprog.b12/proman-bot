package com.b12.proman.project.core.chain;

import com.b12.proman.project.core.Project;
//import com.b12.proman.project.core.ProjectEventWaiterCommand;
import java.util.Scanner;

public class ProjectTitle extends CreateProjectChain {
    //public ProjectEventWaiterCommand projectEventWaiterCommand;
    String output;
    Scanner input = new Scanner(System.in);

    public ProjectTitle() {
        super();
    }

    @Override
    public Project creating(Project project) {
        if (project != null) {
            System.out.println("Set project title: ");
            output = input.nextLine();
            project.setTitle(output);
            //projectEventWaiterCommand.createProjectTitleWaiter(commandEvent);
        }
        if (this.nextProcess != null) {
            this.nextProcess.creating(project);
        }
        return project;
    }
}
