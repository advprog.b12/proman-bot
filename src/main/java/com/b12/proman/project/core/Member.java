package com.b12.proman.project.core;

import com.b12.proman.project.core.informer.MemberInformer;
import com.b12.proman.reminder.core.Reminder;
import java.util.HashMap;
import java.util.Map;
import net.dv8tion.jda.api.entities.User;

public class Member {

    private final String memberId;
    private User user;
    private Map<String, Reminder> reminderTask;
    private Map<String, Boolean> rolesProject;
    private MemberInformer memberInformer;

    public Member(String memberId) {
        this.memberId = memberId;
        reminderTask = new HashMap<>();
        rolesProject = new HashMap<>();
        memberInformer = new MemberInformer(this);
    }

    public String getId() {
        return this.memberId;
    }

    public Map<String, Boolean> getProjects() {
        return rolesProject;
    }

    public Map<String, Reminder> getTasks() {
        return reminderTask;
    }

    public User getUser() {
        return this.user;
    }

    public MemberInformer getMemberInformer() {
        return this.memberInformer;
    }

    public void setProjects(Map<String, Boolean> projects) {
        rolesProject = projects;
    }

    public void setTasks(Map<String, Reminder> tasks) {
        reminderTask = tasks;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void notifyReminder(String nameTask) {
        Reminder reminder = this.reminderTask.get(nameTask);
        reminder.off();
    }

}
