package com.b12.proman.project.core.informer;

import com.b12.proman.project.core.Member;
import com.b12.proman.project.core.Task;

public class MemberInformer {
    
    private Member member;

    public MemberInformer(Member member) {
        this.member = member;
    }

    public void informMember(Task task) {
        this.member.notifyReminder(task.getTitle());
    }

    public Member getMember() {
        return this.member;
    }
}