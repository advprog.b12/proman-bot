package com.b12.proman.project.core.command;

import com.b12.proman.project.core.Project;
import com.b12.proman.project.core.Task;
import com.b12.proman.project.core.chain.CreateProjectChain;
import com.b12.proman.project.core.chain.ProjectDeadline;
import com.b12.proman.project.core.chain.ProjectDescription;
import com.b12.proman.project.core.chain.ProjectMember;
import com.b12.proman.project.core.chain.ProjectTitle;
import com.b12.proman.project.repository.ProjectRepository;
import java.util.List;

public class CreateProjectCommand implements Command {

    CreateProjectChain projectTitle = new ProjectTitle();
    CreateProjectChain projectDescription = new ProjectDescription();
    CreateProjectChain projectMember = new ProjectMember();
    CreateProjectChain projectDeadline = new ProjectDeadline();
    CreateProjectChain projectChain;

    ProjectRepository projectRepository = new ProjectRepository();

    @Override
    public Project createProject(Project project) {
        //TODO

        //Create a project with Chain of Responsibility, at package com.b12.proman.project.core.chain
        projectTitle.setNextProcess(projectDescription);
        projectDescription.setNextProcess(projectMember);
        projectMember.setNextProcess(projectDeadline);
        projectChain = projectTitle;

        if (project != null) {
            //projectRepository.addProject();
            projectChain.creating(project);
        }
        return project;
    }

    @Override
    public Project editProject(Project project) {
        // do nothing
        return null;
    }

    @Override
    public void deleteProject(String nameProject) {
        // do nothing
    }

    @Override
    public Project getProject(String nameProject) {
        // do nothing
        return null;
    }

    @Override
    public List<Project> listProject() {
        // do nothing
        return null;
    }

    @Override
    public Task assignTask(String[] list, String memberId) {
        // do nothing
        return null;
    }

    @Override
    public Task[] listTask() {
        // do nothing
        return null;
    }

    @Override
    public String nameCommand() {
        return "create";
    }
}
