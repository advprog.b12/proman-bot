package com.b12.proman.project.core.observer;

import com.b12.proman.project.core.Member;
import com.b12.proman.project.repository.MemberRepository;
import java.util.TimerTask;
import org.springframework.beans.factory.annotation.Autowired;

public class NotifyTask extends TimerTask {
    @Autowired
    MemberRepository memberRepository;

    private String memberId;
    private String taskTitle;
    private static Member member;

    public NotifyTask(String memberId, String taskTitle) {
        this.memberId = memberId;
        this.taskTitle = taskTitle;
    }

    @Override
    public void run() {
        member = memberRepository.getMemberById(memberId);
        member.notifyReminder(taskTitle);
    }
}