package com.b12.proman.project.core.command;

import com.b12.proman.project.core.Project;
import com.b12.proman.project.core.Task;
import java.util.List;

public class ListTaskCommand implements Command {

    @Override
    public Project createProject(Project project) {
        // do nothing
        return null;
    }

    @Override
    public Project editProject(Project project) {
        //do nothing
        return null;
    }

    @Override
    public void deleteProject(String nameProject) {
        // do nothing

    }

    @Override
    public Project getProject(String nameProject) {
        // do nothing
        return null;
    }

    @Override
    public List<Project> listProject() {
        // do nothing
        return null;
    }

    @Override
    public Task assignTask(String[] list, String memberId) {
        // do nothing
        return null;
    }

    @Override
    public Task[] listTask() {
        //TODO
        return null;
    }

    @Override
    public String nameCommand() {
        return "tasks";
    }
}
