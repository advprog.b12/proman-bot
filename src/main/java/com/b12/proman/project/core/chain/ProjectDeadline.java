package com.b12.proman.project.core.chain;

import com.b12.proman.project.core.Project;
//import com.b12.proman.project.core.ProjectEventWaiterCommand;
import com.b12.proman.project.repository.ProjectRepository;
import java.util.Scanner;

public class ProjectDeadline extends CreateProjectChain {
    String output;
    ProjectRepository projectRepository = new ProjectRepository();
    //public ProjectEventWaiterCommand projectEventWaiterCommand;

    public ProjectDeadline() {
        super();
    }

    Scanner input = new Scanner(System.in);

    @Override
    public Project creating(Project project) {
        if (project != null) {
            System.out.println("Set project deadline: ");
            output = input.nextLine();
            project.setDeadline(output);
            projectRepository.addProject(project);
        }
        if (this.nextProcess != null) {
            this.nextProcess.creating(project);
        }
        return project;
    }
}
