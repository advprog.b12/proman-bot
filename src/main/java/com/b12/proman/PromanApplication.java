package com.b12.proman;

import com.b12.proman.controller.MainController;
import com.b12.proman.project.controller.ProjectEventWaiter;
import com.b12.proman.project.service.ProjectServiceImpl;
import com.jagrosh.jdautilities.command.CommandClientBuilder;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import javax.annotation.PostConstruct;
import javax.security.auth.login.LoginException;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Activity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PromanApplication {

    @Autowired
    MainController mainController;

    private static JDA jda;
    private CommandClientBuilder commandClientBuilder;

    EventWaiter eventWaiter;

    @Autowired
    ProjectServiceImpl projectService;

    private static final String BOT_TOKEN = "ODM3NTYwOTU4Njc4MjA0NDI2.YIuVbQ.2CBnr7zygLX8pxwkr96NXN3Vql0";

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(PromanApplication.class);
        app.run();
    }

    @PostConstruct
    public void run() throws LoginException {
        commandClientBuilder = new CommandClientBuilder();

        createProjectCommands();

        jda = JDABuilder.createDefault(BOT_TOKEN)
                .setStatus(OnlineStatus.DO_NOT_DISTURB)
                .setActivity(Activity.listening("Spotipeh"))
                .addEventListeners(mainController)
                .build();
    }

    public static JDA getJDA() {
        return jda;
    }

    private void createProjectCommands() {
        commandClientBuilder.addCommand(
                new ProjectEventWaiter(projectService)
        );
    }
}
