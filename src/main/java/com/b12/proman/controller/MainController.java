package com.b12.proman.controller;

import com.b12.proman.history.controller.HistoryController;
import com.b12.proman.project.controller.ProjectController;
import com.b12.proman.project.core.Member;
import com.b12.proman.project.repository.MemberRepository;
import com.b12.proman.reminder.controller.ReminderController;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class MainController extends ListenerAdapter {

    @Autowired
    MemberRepository memberRepository;

    @Autowired
    ProjectController projectController;

    @Autowired
    HistoryController historyController;

    @Autowired
    ReminderController reminderController;

    final List<String> projectCommandList = new ArrayList<>(
            Arrays.asList("/create", "/edit", "/delete", "/project", "/projectlist", "/assign-task", "/progress"));
    final List<String> historyCommandList = new ArrayList<>(
            Arrays.asList("/history", "history-project", "history-member"));
    final List<String> taskCommandList = new ArrayList<>(Arrays.asList("/create-task", "task-list", "task-member"));

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {

        String[] args = event.getMessage().getContentRaw().split("\\s+");

        if (!event.getAuthor().isBot()) {

            switch (args[0]) {

                case ("/test"):
                    String result = "woyy";
                    event.getChannel().sendTyping().queue();
                    event.getChannel().sendMessage(result).queue();
                    break;

                case ("/project"):
                    projectController.onGuildMessageReceivedProject(event);
                    break;

                case ("/history"):
                    historyController.onGuildMessageReceivedHistory(event);
                    break;

                case ("/task"):
                    projectController.onGuildMessageReceivedProject(event);
                    break;

                case ("/help"):
                    EmbedBuilder helpEmbed = new EmbedBuilder().setTitle("Help")
                            .setDescription(" '/project' to see the command for project" + "\n"
                                    + "'/history' to see the command for history" + "\n"
                                    + "'/task' to see the command for task" + "\n"
                                    + "'/reminder' to see the command for reminder");
                    helpEmbed.setColor(0x0004ff);

                    event.getChannel().sendTyping().queue();
                    event.getChannel().sendMessage(helpEmbed.build()).queue();
                    break;
                default:

                    break;
            }
        }
    }

    @Override
    public void onPrivateMessageReceived(PrivateMessageReceivedEvent event) {
        if (!event.getAuthor().isBot()) {
            reminderController.onPrivateMessageReceivedReminder(event);
        }
    }

    @Override
    public void onGuildMemberJoin(GuildMemberJoinEvent event) {
        String memberId = event.getMember().getId();
        memberRepository.addMember(new Member(memberId));
    }

    // @Autowired
    // public void createProject(CommandEvent event){
    // String[] args = event.getMessage().getContentRaw().split("\\s+");
    // if (Objects.equals(args[0], "/project-createee")){
    // projectCreateController.execute(event);
    // }
    //
    // }
}
