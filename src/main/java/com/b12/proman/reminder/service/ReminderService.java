package com.b12.proman.reminder.service;

import com.b12.proman.reminder.core.Reminder;
import net.dv8tion.jda.api.entities.User;

public interface ReminderService {

    public String setReminder(String title, String mode, String... args);
    
    public String setOn(String title);

    public String setOff(String title);

    // Fixme: kembaliin lagi ke implementasi sebelumnya, String add(Reminder reminder)
    // public String add(String title, User user);

    public String add(Reminder reminder);
}
