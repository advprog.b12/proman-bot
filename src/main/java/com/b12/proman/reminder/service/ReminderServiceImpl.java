package com.b12.proman.reminder.service;

import com.b12.proman.reminder.core.Reminder;
import com.b12.proman.reminder.repository.ReminderRepository;
import com.b12.proman.reminder.service.changer.ReminderChanger;
import net.dv8tion.jda.api.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReminderServiceImpl implements ReminderService {
    // TODO: bikin add Reminder ke Repository 
    // TODO: automatically nunjukin detail task waktu reminder

    @Autowired
    ReminderRepository reminderRepository;

    @Autowired
    ReminderChanger reminderChanger;

    @Override
    public String setReminder(String title, String mode, String...args) {

        Reminder reminder = reminderRepository.findByTitle(title);
        
        switch (mode.toLowerCase()) {
            case "-h":
                reminderChanger.coordinate("-h");
                reminderChanger.changeStrategy(reminder, args);
                break;

            case "-d":
                reminderChanger.coordinate("-d");
                reminderChanger.changeStrategy(reminder, args);
                break;

            case "-w":
                reminderChanger.coordinate("-w");
                reminderChanger.changeStrategy(reminder, args);
                break;
        
            case "-m":
                reminderChanger.coordinate("-m");
                reminderChanger.changeStrategy(reminder, args);
                break;

            case "-c":
                reminderChanger.coordinate("-c");
                reminderChanger.changeStrategy(reminder, args);
                break;
                
            default:
                return "❌ Your argument is invalid!";
        }

        return String.format("✅ Succesfully change your reminder!!%n%s", reminder.getInfo());
    }

    @Override
    public String setOn(String title) {
        Reminder reminder = reminderRepository.findByTitle(title);

        if (reminder.isOn()) {
            return title + " is already on";
        }

        reminder.on();
        return title + " is switched on";
    }

    @Override
    public String setOff(String title) {
        Reminder reminder = reminderRepository.findByTitle(title);

        if (!reminder.isOn()) {
            return title + " is already off";
        }

        reminder.off();
        return title + " is switched off";
    }

    // FIXME: ubah method ini ke implementasi sebelumnya String add(Reminder reminder)
    @Override
    public String add(Reminder reminder) {
        // Reminder reminder = new Reminder(title, user);
        reminderRepository.add(reminder);

        return "Reminder created";
    }
}