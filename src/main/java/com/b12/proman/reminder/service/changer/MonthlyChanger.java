package com.b12.proman.reminder.service.changer;

import com.b12.proman.reminder.core.Reminder;
import com.b12.proman.reminder.core.strategy.MonthlyReminder;

public class MonthlyChanger extends ReminderChanger {

    @Override
    public void changeStrategy(Reminder reminder, String... args) {
        if ((args.length != 0)) {
            var startingTime = Integer.parseInt(args[0]);
            reminder.setReminder(new MonthlyReminder(startingTime));
        } else {
            reminder.setReminder(new MonthlyReminder());
        }
    }
    
}
