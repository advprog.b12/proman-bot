package com.b12.proman.reminder.service.changer;

import com.b12.proman.reminder.core.Reminder;
import com.b12.proman.reminder.core.strategy.DailyReminder;

public class DailyChanger extends ReminderChanger {

    @Override
    public void changeStrategy(Reminder reminder, String... args) {
        if ((args.length != 0)) {
            var startingTime = Integer.parseInt(args[0]);
            reminder.setReminder(new DailyReminder(startingTime));
        } else {
            reminder.setReminder(new DailyReminder());
        }
    }
    
}
