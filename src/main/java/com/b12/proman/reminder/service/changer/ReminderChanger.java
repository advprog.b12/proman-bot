package com.b12.proman.reminder.service.changer;

import com.b12.proman.reminder.core.Reminder;
import org.springframework.stereotype.Component;

@Component
public class ReminderChanger {

    private ReminderChanger changer;

    public void changeStrategy(Reminder reminder, String... args) {
        this.changer.changeStrategy(reminder, args);
    }

    public void coordinate(String mode) {
        
        switch (mode) {

            case "-h":
                this.changer = new HourlyChanger();
                break;

            case "-d":
                this.changer = new DailyChanger();
                break;
                
            case "-w":
                this.changer = new WeeklyChanger();
                break;
                
            case "-m":
                this.changer = new MonthlyChanger();
                break;

            case "-c":
                this.changer = new CustomChanger();
                break;

            default:
                break;
        }
    }

    public ReminderChanger getChanger() {
        return this.changer;
    }
}