package com.b12.proman.reminder.service.changer;

import com.b12.proman.reminder.core.Reminder;
import com.b12.proman.reminder.core.strategy.CustomReminder;

public class CustomChanger extends ReminderChanger {

    @Override
    public void changeStrategy(Reminder reminder, String... args) {
        var periodTime = Integer.parseInt(args[0].substring(0, 1));
        var periodTimeUnit = args[0].substring(1);

        if (args.length > 1) {
            var startingTime = Integer.parseInt(args[1]);
            reminder.setReminder(new CustomReminder(periodTime, periodTimeUnit, startingTime));
        } else {
            reminder.setReminder(new CustomReminder(periodTime, periodTimeUnit));
        }
    }
    
}
