package com.b12.proman.reminder.core.strategy;

import com.b12.proman.reminder.core.functional.PrivateMessaging;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import net.dv8tion.jda.api.entities.User;

public class HourlyReminder implements ReminderStrategy {

    private Timer timer;
    private TimerTask task;
    private Date dateStart;
    private final Duration fixedDuration = Duration.of(1, ChronoUnit.HOURS);
    
    public HourlyReminder(int hourOfDay) {
        dateStart = convertToDate(hourOfDay);
    }

    public HourlyReminder() {
        dateStart = new Date();
    }
    
    @Override
    public void remind(User user) {
        timer = createTimer();
        task = createTask(user);
        timer.scheduleAtFixedRate(task, dateStart, fixedDuration.toMillis());
    }

    @Override
    public void stop() {
        timer.cancel();
    }

    @Override
    public String getInfo() {
        return String.format("Starting date: %s %nReminder period: Hourly", dateStart.toString());
    }

    public Timer getTimer() {
        return this.timer;
    }

    public TimerTask getTask() {
        return this.task;
    }

    public Date getDateStart() {
        return this.dateStart;
    }

    public Timer createTimer() {
        return new Timer();
    }

    public TimerTask createTask(User user) {
        return new PrivateMessaging(user);
    }

    protected Date convertToDate(int hourOfDay) {
        var date = Calendar.getInstance();
        date.set(Calendar.HOUR_OF_DAY, hourOfDay);
        return date.getTime();
    }

    @Override
    public String toString() {
        return "Hourly Reminder";
    }
}
