package com.b12.proman.reminder.core.strategy;

import net.dv8tion.jda.api.entities.User;

public interface ReminderStrategy {

    public void remind(User user);

    public void stop();

    public String getInfo();
}
