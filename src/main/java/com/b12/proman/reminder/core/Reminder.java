package com.b12.proman.reminder.core;

import com.b12.proman.reminder.core.strategy.DailyReminder;
import com.b12.proman.reminder.core.strategy.ReminderStrategy;
import java.util.concurrent.ThreadLocalRandom;
import net.dv8tion.jda.api.entities.User;

// FIXME: ubah lagi implementasinya, antara Reminder sama ReminderStrategy
// TODO: buat atribut user di Reminder dan bikin method on() ngepass PrivateMessaging(user)
// ke reminder strategy
public class Reminder {

    private String title;
    private ReminderStrategy reminderStrategy;
    private User user;
    private boolean isOn;

    public Reminder(String title, ReminderStrategy reminderStrategy, User user) {
        this.title = title;
        this.reminderStrategy = reminderStrategy;
        this.user = user;
        on();
    }

    public Reminder(User user) {
        // Auto-generated name
        title = "Reminder" + ThreadLocalRandom.current().nextInt(0, 10000);

        // Daily reminder as a default reminder
        reminderStrategy = new DailyReminder();
        
        this.user = user;
        on();
    }
    
    public Reminder(String title, User user) {
        this(title, new DailyReminder(), user);
    }
    
    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return this.title;
    }

    public void setReminder(ReminderStrategy reminderStrategy) {
        off();
        this.reminderStrategy = reminderStrategy;
        on();
    }

    public ReminderStrategy getStrategy() {
        return this.reminderStrategy;
    }

    public void on() {
        isOn = true;
        this.reminderStrategy.remind(user);
    }

    public void off() {
        isOn = false;
        this.reminderStrategy.stop();
    }

    public boolean isOn() {
        return this.isOn;
    }

    public String getInfo() {
        return String.format("Title: %s %n %s", this.getTitle(), this.reminderStrategy.getInfo());
    }

    @Override
    public String toString() {
        return "Title: " + this.title + "\nType: " + this.reminderStrategy;
    }
}
