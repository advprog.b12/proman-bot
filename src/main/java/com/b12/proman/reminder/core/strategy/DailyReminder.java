package com.b12.proman.reminder.core.strategy;

import com.b12.proman.reminder.core.functional.PrivateMessaging;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import net.dv8tion.jda.api.entities.User;

public class DailyReminder implements ReminderStrategy {
    
    private Timer timer;
    private TimerTask task;
    private Date dateStart;
    private final Duration fixedDuration = Duration.of(1, ChronoUnit.DAYS);

    public DailyReminder(int hour) {
        dateStart = convertToDate(hour);
    }

    public DailyReminder() {
        dateStart = new Date();
    }

    @Override
    public void remind(User user) {
        timer = createTimer();
        task = createTask(user);
        timer.scheduleAtFixedRate(task, dateStart, fixedDuration.toMillis());
    }

    @Override
    public void stop() {
        timer.cancel();
    }

    @Override
    public String getInfo() {
        return String.format("Starting date: %s %nReminder period: Daily", dateStart.toString());
    }

    public Timer createTimer() {
        return new Timer();
    }

    public TimerTask createTask(User user) {
        return new PrivateMessaging(user);
    }

    private Date convertToDate(int hour) {
        var date = Calendar.getInstance();
        date.set(Calendar.HOUR_OF_DAY, hour);
        return date.getTime();
    }

    @Override
    public String toString() {
        return "Daily Reminder\nDate= " + dateStart + "\nDuration= " + fixedDuration;
    }
}
