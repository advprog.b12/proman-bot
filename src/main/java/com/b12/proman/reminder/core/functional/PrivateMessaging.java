package com.b12.proman.reminder.core.functional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.TimerTask;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.User;

public class PrivateMessaging extends TimerTask {

    private User user;
    Random random = new Random();
    EmbedBuilder reminder = new EmbedBuilder();

    List<String> listOfResponse = new ArrayList<>(
        Arrays.asList(
            "Remember! You have one job! Or maybe a couple. Check it in your project ;)",
            "Don't forget to get your task done.",
            "A friendly reminder. That task won't be finished by itself. Then finish them 😊. For you.",
            "You ask me to remind you. Here you go. There's a task to be done."
        )
    );

    public PrivateMessaging(User user) {
        this.user = user;
    }

    @Override
    public void run() {
        user.openPrivateChannel().queue(
            privateChannel -> {
                reminder.setTitle("⏰ Reminder")
                        .setDescription(randomPickResponse())
                        .setColor(0xf0d405);
                privateChannel.sendMessage(reminder.build()).queue();
            }
        );
        reminder.clear();
    }

    public String randomPickResponse() {
        return listOfResponse.get(random.nextInt(listOfResponse.size()));
    }
}
