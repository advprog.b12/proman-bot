package com.b12.proman.reminder.core.strategy;

import com.b12.proman.reminder.core.functional.PrivateMessaging;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import net.dv8tion.jda.api.entities.User;

public class WeeklyReminder implements ReminderStrategy {
    
    private Timer timer;
    private TimerTask task;
    private Date dateStart;

    /**
     * Duration cant be in WEEKS, so firstly convert it to DAYS.
     */
    private final Duration fixedDuration = Duration.of(7, ChronoUnit.DAYS);

    public WeeklyReminder(int dayOfWeek) {
        dateStart = convertToDate(dayOfWeek);
    }

    public WeeklyReminder() {
        dateStart = new Date();
    }

    @Override
    public void remind(User user) {
        timer = createTimer();
        task = createTask(user);
        timer.scheduleAtFixedRate(task, dateStart, fixedDuration.toMillis());
    }

    @Override
    public void stop() {
        timer.cancel();
    }

    @Override
    public String getInfo() {
        return String.format("Starting date: %s %nReminder period: Weekly", dateStart.toString());
    }

    public Timer createTimer() {
        return new Timer();
    }

    public TimerTask createTask(User user) {
        return new PrivateMessaging(user);
    }

    private Date convertToDate(int dayOfWeek) {
        var date = Calendar.getInstance();
        date.set(Calendar.DAY_OF_WEEK, dayOfWeek);
        return date.getTime();
    }

    @Override
    public String toString() {
        return "Weekly Reminder";
    }
}
