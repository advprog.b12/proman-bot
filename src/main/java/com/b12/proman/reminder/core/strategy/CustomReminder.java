package com.b12.proman.reminder.core.strategy;

import com.b12.proman.reminder.core.functional.PrivateMessaging;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import net.dv8tion.jda.api.entities.User;

public class CustomReminder implements ReminderStrategy {
    
    private Timer timer;
    private TimerTask task;
    private Date dateStart;
    private Duration fixedDuration;
    private String durationString;

    /**
     * @param timeUnit is either mi, h, d, w, m each 
     * represents Minutes, Hour, Day, Week, Month.
     * @param dateStart is the date the user set for reminder to start.
     * 
     */
    public CustomReminder(int period, String timeUnit, int dateStart) {
        this.dateStart = convertToDate(timeUnit, dateStart);
        setDuration(period, timeUnit);
    }

    public CustomReminder(int period, String timeUnit) {
        dateStart = new Date();
        setDuration(period, timeUnit);
    }

    @Override
    public void remind(User user) {
        timer = createTimer();
        task = createTask(user);
        timer.scheduleAtFixedRate(task, dateStart, fixedDuration.toMillis());
    }

    @Override
    public void stop() {
        timer.cancel();
    }

    @Override
    public String getInfo() {
        return String.format("Starting date: %s %nReminder period: %s", dateStart.toString(), durationString);
    }

    public Timer createTimer() {
        return new Timer();
    }

    public TimerTask createTask(User user) {
        return new PrivateMessaging(user);
    }

    /**
     * @implNote the "mi" timeUnit which represents Minute is only for testing purpose.
     */
    private void setDuration(int period, String timeUnit) {

        switch (timeUnit) {

            case "mi":
                fixedDuration = Duration.of(period, ChronoUnit.MINUTES);
                durationString = String.format("%s Minute(s)", period);
                break;
            
            case "h":
                fixedDuration = Duration.of(period, ChronoUnit.HOURS);
                durationString = String.format("%s Hour(s)", period);
                break;

            case "d":
                fixedDuration = Duration.of(period, ChronoUnit.DAYS);
                durationString = String.format("%s Day(s)", period);
                break;

            case "w":
                fixedDuration = Duration.of(7 * period, ChronoUnit.DAYS);
                durationString = String.format("%s Week(s)", period);
                break;

            case "m":
                fixedDuration = Duration.of(30 * period, ChronoUnit.DAYS);
                durationString = String.format("%s Month(s)", period);
                break;

            default:
                break;
        }
    }

    /**
     * for the time being user can only set @param dateStart 
     * according to @param timeUnit.
     * 
     */
    private Date convertToDate(String timeUnit, int dateStart) {
        var date = Calendar.getInstance();
        
        switch (timeUnit) {
            case "mi":
                date.set(Calendar.MINUTE, dateStart);
                break;
            case "h":
                date.set(Calendar.HOUR_OF_DAY, dateStart);
                break;
            case "d":
                date.set(Calendar.HOUR_OF_DAY, dateStart);
                break;
            case "w":
                date.set(Calendar.DAY_OF_WEEK, dateStart);
                break;
            case "m":
                date.set(Calendar.DAY_OF_MONTH, dateStart);
                break;
            default:
                break;
        }

        return date.getTime();
    }

    @Override
    public String toString() {
        return "Custom Reminder";
    }
}