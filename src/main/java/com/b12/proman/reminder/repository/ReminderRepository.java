package com.b12.proman.reminder.repository;

import com.b12.proman.reminder.core.Reminder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;

@Repository
public class ReminderRepository {
    private Map<String, Reminder> reminders = new HashMap<>();

    public List<Reminder> findAll() {
        return new ArrayList<>(reminders.values());
    }

    public Reminder findByTitle(String title) {
        return reminders.get(title);
    }

    public void add(Reminder reminder) {
        if (reminders.get(reminder.getTitle()) == null) {
            String reminderTitle = reminder.getTitle();
            reminders.put(reminderTitle, reminder);
        }
    }
}