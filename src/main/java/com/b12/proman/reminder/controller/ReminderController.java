package com.b12.proman.reminder.controller;

import com.b12.proman.reminder.core.Reminder;
import com.b12.proman.reminder.service.ReminderService;
import java.util.Arrays;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class ReminderController {
    
    @Autowired
    ReminderService reminderService;

    public void onPrivateMessageReceivedReminder(PrivateMessageReceivedEvent event) {

        String[] query = event.getMessage().getContentRaw().split("\\s+");

        User user = event.getAuthor();
        
        String command = query[0];

        String title = query[1];

        String answer;

        switch (command) {
            case "/change-reminder":
                String mode = query[2];
                String[] args = Arrays.copyOfRange(query, 3, query.length);
                answer = reminderService.setReminder(title, mode, args);
                event.getChannel().sendTyping().queue();
                event.getChannel().sendMessage(answer).queue();
                break;
            
            case "/set-on":
                answer = reminderService.setOn(title);
                event.getChannel().sendTyping().queue();
                event.getChannel().sendMessage(answer).queue();
                break;

            case "/set-off":
                answer = reminderService.setOff(title);
                event.getChannel().sendTyping().queue();
                event.getChannel().sendMessage(answer).queue();
                break;

            // XXX: command yang harusnya gaada, tau deh dibiarin aja apa ngga
            case "/add-reminder":
                Reminder reminder = new Reminder(title, user);
                answer = reminderService.add(reminder);
                event.getChannel().sendTyping().queue();
                event.getChannel().sendMessage(answer).queue();
                break;

            default:
                break;
        }
    }
}
