package com.b12.proman.history.repository;

import com.b12.proman.history.core.History;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import org.springframework.stereotype.Repository;

@Repository
public class HistoryRepository {

    public ArrayList<History> historyList;
    public DateTimeFormatter myFormatObj;

    public HistoryRepository() {
        this.historyList = new ArrayList<>();
        this.myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        ///addDummyHistory();
    }

    public ArrayList<History> getAllHistoryList() {
        return this.historyList;
    }

    public ArrayList<History> getHistoryByMemberName(String member) {
        ArrayList<History> tempHistory = new ArrayList<>();
        int counter1 = 0;

        for (History h : this.historyList) {
            if (counter1 < 10) {
                if (h.getHistoryMember().equals(member)) {
                    tempHistory.add(h);
                    counter1 += 1;
                }
            }
        }

        return tempHistory;
    }

    public ArrayList<History> getHistoryByProjectTitle(String title) {
        ArrayList<History> tempHistory = new ArrayList<>();
        int counter2 = 0;

        for (History h : this.historyList) {
            if (counter2 < 10) {
                if (h.getHistoryProject().equals(title)) {
                    tempHistory.add(h);
                    counter2 += 1;
                }
            }
        }

        return tempHistory;
    }

    public ArrayList<History> getHistoryByLogDate(String date) {
        ArrayList<History> tempHistory = new ArrayList<>();
        int counter3 = 0;

        for (History h : this.historyList) {
            if (counter3 < 10) {
                if (h.getHistoryDate().equals(date)) {
                    tempHistory.add(h);
                    counter3 += 1;
                }
            }
        }

        return tempHistory;
    }

    public void addNewHistory(String member, String project, String description) {

        String datetime = LocalDateTime.now().format(this.myFormatObj);
        String[] splitted = datetime.split(" ");
        String date = splitted[0];
        String time = splitted[1];

        History newHistory = new History(member, project, description, date, time);
        this.historyList.add(0, newHistory);
    }

    public void clearHistoryList() {
        this.historyList.clear();
    }

    public void addDummyHistory() {

        String member1 = "Ihwan";
        String project1 = "TK Advprog";
        String description1 = "Changed something in project package...";
        String date1 = "10-05-2021";
        String time1 = "15.55.55";

        History newHistory1 = new History(member1, project1, description1, date1, time1);
        this.historyList.add(newHistory1);

        String member2 = "AlHazairin";
        String project2 = "Proyek Akhir";
        String description2 = "Made a few changes in some files...";
        String date2 = "08-05-2021";
        String time2 = "10.15.30";

        History newHistory2 = new History(member2, project2, description2, date2, time2);
        this.historyList.add(newHistory2);

        String member3 = "prim";
        String project3 = "Tugas Akhir Sadat";
        String description3 = "Created new files in the repository...";
        String date3 = "05-05-2021";
        String time3 = "12.24.36";

        History newHistory3 = new History(member3, project3, description3, date3, time3);
        this.historyList.add(newHistory3);

        String member4 = "dommyummy";
        String project4 = "Tugas Kelompok Anum";
        String description4 = "Updated a few lines in the repository...";
        String date4 = "03-05-2021";
        String time4 = "11.22.33";

        History newHistory4 = new History(member4, project4, description4, date4, time4);
        this.historyList.add(newHistory4);

        String member5 = "fadli";
        String project5 = "TK Advprog";
        String description5 = "Do something with the .gitlab-ci.yml...";
        String date5 = "02-05-2021";
        String time5 = "20.30.40";

        History newHistory5 = new History(member5, project5, description5, date5, time5);
        this.historyList.add(newHistory5);

        String member6 = "Danin";
        String project6 = "Asdos Advprog";
        String description6 = "Finished project demo and give scores...";
        String date6 = "01-05-2021";
        String time6 = "21.15.45";

        History newHistory6 = new History(member6, project6, description6, date6, time6);
        this.historyList.add(newHistory6);
    }

    public void addDummyHistoryWithMemberName(String member) {
        String project1 = "Random Project";
        String description1 = "Randomly changed something by accident...";

        String datetime1 = LocalDateTime.now().format(this.myFormatObj);
        String[] splitted = datetime1.split(" ");
        String date1 = splitted[0];
        String time1 = splitted[1];

        History newHistory1 = new History(member, project1, description1, date1, time1);
        this.historyList.add(newHistory1);
    }

    public void addDummyHistoryWithProjectTitle(String title) {
        String member1 = "Ihwanes";
        String description1 = "Created a new project...";

        String datetime1 = LocalDateTime.now().format(this.myFormatObj);
        String[] splitted = datetime1.split(" ");
        String date1 = splitted[0];
        String time1 = splitted[1];

        History newHistory1 = new History(member1, title, description1, date1, time1);
        this.historyList.add(newHistory1);
    }
}
