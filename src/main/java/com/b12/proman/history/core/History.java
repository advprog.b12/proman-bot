package com.b12.proman.history.core;

public class History {
    private String member;
    private String project;
    private String description;
    private String date;
    private String time;

    public History(String member, String project, String description, String date, String time) {
        this.member = member;
        this.project = project;
        this.description = description;
        this.date = date;
        this.time = time;
    }

    public void setHistoryMember(String member) {
        this.member = member;
    }

    public void setHistoryProject(String project) {
        this.project = project;
    }

    public void setHistoryDescription(String description) {
        this.description = description;
    }

    public void setHistoryDate(String date) {
        this.date = date;
    }

    public void setHistoryTime(String time) {
        this.time = time;
    }

    public String getHistoryMember() {
        return this.member;
    }

    public String getHistoryProject() {
        return this.project;
    }

    public String getHistoryDescription() {
        return this.description;
    }

    public String getHistoryDate() {
        return this.date;
    }

    public String getHistoryTime() {
        return this.time;
    }
}
