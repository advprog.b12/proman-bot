package com.b12.proman.history.service;

import com.b12.proman.history.core.History;
import com.b12.proman.history.repository.HistoryRepository;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HistoryServiceImpl implements HistoryService {

    @Autowired
    HistoryRepository historyRepository;

    @Override
    public ArrayList<History> getAllHistory() {
        return historyRepository.getAllHistoryList();
    }

    @Override
    public ArrayList<History> getHistoryByMember(String member) {
        ///historyRepository.addDummyHistoryWithMemberName(member);
        return historyRepository.getHistoryByMemberName(member);
    }

    @Override
    public ArrayList<History> getHistoryByProject(String title) {
        ///historyRepository.addDummyHistoryWithProjectTitle(title);
        return historyRepository.getHistoryByProjectTitle(title);
    }

    @Override
    public ArrayList<History> getHistoryByDate(String date) {
        ///historyRepository.addDummyHistoryWithProjectTitle(date);
        return historyRepository.getHistoryByLogDate(date);
    }

    @Override
    public void addHistory(String member, String project, String desc) {
        historyRepository.addNewHistory(member, project, desc);
    }

    @Override
    public void clearHistory() {
        historyRepository.clearHistoryList();
    }

    @Override
    public void addHistoryDummy() {
        historyRepository.addDummyHistory();
    }

}
