package com.b12.proman.history.service;

import com.b12.proman.history.core.History;
import java.util.ArrayList;

public interface HistoryService {

    ArrayList<History> getAllHistory();

    ArrayList<History> getHistoryByMember(String member);

    ArrayList<History> getHistoryByProject(String title);

    ArrayList<History> getHistoryByDate(String date);

    void addHistory(String member, String project, String desc);

    void clearHistory();

    void addHistoryDummy();
    
}
