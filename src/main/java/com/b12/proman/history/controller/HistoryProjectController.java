package com.b12.proman.history.controller;

import com.b12.proman.history.core.History;
import com.b12.proman.history.service.HistoryService;
import java.util.ArrayList;
import java.util.Arrays;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HistoryProjectController {

    @Autowired
    private HistoryService historyService;

    public void onGuildMessageReceivedHistoryProject(GuildMessageReceivedEvent event) {

        String[] args = event.getMessage().getContentRaw().split("\\s+");
        String author = event.getMember().getEffectiveName();
        EmbedBuilder historyEmbed;

        if (args.length > 2) {

            String[] titles = Arrays.copyOfRange(args, 2, args.length);
            String title = String.join(" ", titles);

            ArrayList<History> histories = historyService.getHistoryByProject(title);

            if (histories.size() > 0) {
                
                historyEmbed = new EmbedBuilder();
                historyEmbed.setTitle("💽 History of Project: " + title);
                historyEmbed.setDescription("A history of recent updates and changes made in the project titled [" + title + "]");
                historyEmbed.setColor(0x0004FF);
                historyEmbed.setFooter("Called by " + author, event.getAuthor().getAvatarUrl());

                for (History h : histories) {
                    historyEmbed.addField(h.getHistoryProject(),
                            h.getHistoryDescription() + "\nAuthored by: " + h.getHistoryMember()
                                    + "\nat Date: " + h.getHistoryDate() + "\nat Time: " + h.getHistoryTime(),
                            false);
                }

                event.getChannel().sendTyping().queue();
                event.getChannel().sendMessage(historyEmbed.build()).queue();

            } else {

                historyEmbed = new EmbedBuilder();
                historyEmbed.setTitle("⚠️ Error: No Project Found");
                historyEmbed.setDescription("There was no project was found with that name \n"
                        + "Or no recent updates/logs were made for that project");
                historyEmbed.setColor(0xF1C40F);

                event.getChannel().sendTyping().queue();
                event.getChannel().sendMessage(historyEmbed.build()).queue();
            }

        } else {
            historyEmbed = new EmbedBuilder();
            historyEmbed.setTitle("❌ Error: Invalid Command Inputs");
            historyEmbed.setDescription("The command or parameter(s) you inputted was invalid or doesn't exist \n"
                    + "Try using the '/history -help' command to see all valid inputs");
            historyEmbed.setColor(0xE74C3C);

            event.getChannel().sendTyping().queue();
            event.getChannel().sendMessage(historyEmbed.build()).queue();
        }
    }
}
