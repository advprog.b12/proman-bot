package com.b12.proman.history.controller;

import com.b12.proman.history.core.History;
import com.b12.proman.history.service.HistoryService;
import java.util.ArrayList;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HistoryDateController {

    @Autowired
    private HistoryService historyService;

    public void onGuildMessageReceivedHistoryDate(GuildMessageReceivedEvent event) {

        String[] args = event.getMessage().getContentRaw().split("\\s+");
        String author = event.getMember().getEffectiveName();
        EmbedBuilder historyEmbed;

        if (args.length == 3) {

            String date = args[2];
            ArrayList<History> histories = historyService.getHistoryByDate(date);

            if (histories.size() > 0) {

                historyEmbed = new EmbedBuilder();
                historyEmbed.setTitle("📅 History of Projects from: " + date);
                historyEmbed.setDescription("A history of updates and changes made during [" + date + "]");
                historyEmbed.setColor(0x0004FF);
                historyEmbed.setFooter("Called by " + author, event.getAuthor().getAvatarUrl());

                for (History h : histories) {
                    historyEmbed.addField(h.getHistoryProject(),
                            h.getHistoryDescription() + "\nAuthored by: " + h.getHistoryMember()
                                    + "\nat Date: " + h.getHistoryDate() + "\nat Time: " + h.getHistoryTime(),
                            false);
                }

                event.getChannel().sendTyping().queue();
                event.getChannel().sendMessage(historyEmbed.build()).queue();

            } else {

                historyEmbed = new EmbedBuilder();
                historyEmbed.setTitle("⚠️ Error: No History Found");
                historyEmbed.setDescription("No history or recent updates/logs made during that date \n"
                        + "Or you might have put in the wrong date format as input \n"
                        + "Try using the format: '/history -date <dd-MM-yyyy>' ");
                historyEmbed.setColor(0xF1C40F);

                event.getChannel().sendTyping().queue();
                event.getChannel().sendMessage(historyEmbed.build()).queue();
            }

        } else {

            historyEmbed = new EmbedBuilder();
            historyEmbed.setTitle("❌ Error: Invalid Command Inputs");
            historyEmbed.setDescription("The command or parameter(s) you inputted was invalid or doesn't exist \n"
                    + "Try using the '/history -help' command to see all valid inputs");
            historyEmbed.setColor(0xE74C3C);

            event.getChannel().sendTyping().queue();
            event.getChannel().sendMessage(historyEmbed.build()).queue();
        }
    }
}
