package com.b12.proman.history.controller;

import com.b12.proman.history.core.History;
import com.b12.proman.history.service.HistoryService;
import java.util.ArrayList;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HistoryController {

    @Autowired
    private HistoryService historyService;

    @Autowired
    private HistoryMemberController memberController;

    @Autowired
    private HistoryProjectController projectController;

    @Autowired
    private HistoryDateController dateController;

    public void onGuildMessageReceivedHistory(GuildMessageReceivedEvent event) {

        String[] args = event.getMessage().getContentRaw().split("\\s+");
        String author = event.getMember().getEffectiveName();
        EmbedBuilder historyEmbed;

        if (args.length == 1) {

            historyEmbed = new EmbedBuilder();
            historyEmbed.setTitle("📜 All History");
            historyEmbed.setDescription("A collection filled with the history of recent updates and changes made by everyone");
            historyEmbed.setColor(0x0004FF);
            historyEmbed.setFooter("Called by " + author, event.getAuthor().getAvatarUrl());
            ArrayList<History> histories = historyService.getAllHistory();

            for (History h : histories) {
                historyEmbed.addField(h.getHistoryProject(),
                        h.getHistoryDescription() + "\nAuthored by: " + h.getHistoryMember()
                                + "\nat Date: " + h.getHistoryDate() + "\nat Time: " + h.getHistoryTime(),
                        false);
            }

            event.getChannel().sendTyping().queue();
            event.getChannel().sendMessage(historyEmbed.build()).queue();

        } else {
            switch (args[1]) {

                case ("-member"):
                    memberController.onGuildMessageReceivedHistoryMember(event);
                    break;

                case ("-project"):
                    projectController.onGuildMessageReceivedHistoryProject(event);
                    break;

                case ("-date"):
                    dateController.onGuildMessageReceivedHistoryDate(event);
                    break;

                case ("-add"):
                    break;

                case ("-clear"):
                    historyService.clearHistory();
                    break;

                case ("-addDummy"):
                    historyService.addHistoryDummy();
                    break;

                case ("-help"):
                    historyEmbed = new EmbedBuilder();
                    historyEmbed.setTitle("ℹ️ List of History Commands");
                    historyEmbed.setDescription("/history : Return a list of all recent updates \n"
                            + " /history -member <@MemberName> : Return updates made by a member \n"
                            + " /history -project <Project_Name> : Return updates made in a project \n"
                            + " /history -date <DD-MM-YYYY> : Return updates made during a date \n"
                            + " /history -clear : Delete all recent history from the repository");
                    historyEmbed.setColor(0x8E44AD);

                    event.getChannel().sendTyping().queue();
                    event.getChannel().sendMessage(historyEmbed.build()).queue();
                    break;

                default:
                    historyEmbed = new EmbedBuilder();
                    historyEmbed.setTitle("❌ Error: Invalid Command Inputs");
                    historyEmbed.setDescription("The command or parameter(s) you inputted was invalid \n"
                            + "Try using the '/history -help' command to see all valid inputs");
                    historyEmbed.setColor(0xE74C3C);

                    event.getChannel().sendTyping().queue();
                    event.getChannel().sendMessage(historyEmbed.build()).queue();
                    break;
            }
        }
    }
}
