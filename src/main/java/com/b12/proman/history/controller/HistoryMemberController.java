package com.b12.proman.history.controller;

import com.b12.proman.history.core.History;
import com.b12.proman.history.service.HistoryService;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HistoryMemberController {

    @Autowired
    private HistoryService historyService;

    public void onGuildMessageReceivedHistoryMember(GuildMessageReceivedEvent event) {

        String[] args = event.getMessage().getContentRaw().split("\\s+");
        String author = event.getMember().getEffectiveName();
        EmbedBuilder historyEmbed;

        if (args.length > 2) {

            List<Member> member = event.getMessage().getMentionedMembers();

            if (member.size() > 0) {

                String name = member.get(0).getEffectiveName();

                historyEmbed = new EmbedBuilder();
                historyEmbed.setTitle("👨‍ History of Member: " + name);
                historyEmbed.setDescription("A history of recent updates and changes made by member [" + name + "]");
                historyEmbed.setColor(0x0004FF);
                historyEmbed.setFooter("Called by " + author, event.getAuthor().getAvatarUrl());

                ArrayList<History> histories = historyService.getHistoryByMember(name);
                for (History h : histories) {
                    historyEmbed.addField(h.getHistoryProject(),
                            h.getHistoryDescription() + "\nAuthored by: " + h.getHistoryMember()
                                    + "\nat Date: " + h.getHistoryDate() + "\nat Time: " + h.getHistoryTime(),
                            false);
                }

                event.getChannel().sendTyping().queue();
                event.getChannel().sendMessage(historyEmbed.build()).queue();

            } else {

                historyEmbed = new EmbedBuilder();
                historyEmbed.setTitle("⚠️ Error: No Member Found");
                historyEmbed.setDescription("There was no member was found with that name \n"
                        + "Or no recent updates/logs were made by that member");
                historyEmbed.setColor(0xF1C40F);

                event.getChannel().sendTyping().queue();
                event.getChannel().sendMessage(historyEmbed.build()).queue();
            }

        } else {

            historyEmbed = new EmbedBuilder();
            historyEmbed.setTitle("❌ Error: Invalid Command Inputs");
            historyEmbed.setDescription("The command or parameter(s) you inputted was invalid or doesn't exist \n"
                    + "Try using the '/history -help' command to see all valid inputs");
            historyEmbed.setColor(0xE74C3C);

            event.getChannel().sendTyping().queue();
            event.getChannel().sendMessage(historyEmbed.build()).queue();
        }
    }
}
