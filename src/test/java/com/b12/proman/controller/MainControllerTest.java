package com.b12.proman.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.b12.proman.history.controller.HistoryController;
import com.b12.proman.project.controller.ProjectController;
import com.b12.proman.project.repository.MemberRepository;
import com.b12.proman.reminder.controller.ReminderController;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import net.dv8tion.jda.internal.requests.RestActionImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class MainControllerTest {

    @InjectMocks
    MainController mainController;

    @Spy
    MemberRepository memberRepository;

    @Spy
    ProjectController projectController;

    @Spy
    HistoryController historyController;

    @Spy
    ReminderController reminderController;

    @Test
    public void testOnGuildMessageReceivedOnTest() throws Exception {
        GuildMessageReceivedEvent messageReceivedEvent = mock(GuildMessageReceivedEvent.class);
        Message message = mock(Message.class);
        User user = mock(User.class);
        TextChannel textChannel = mock(TextChannel.class);
        MessageAction messageAction = mock(MessageAction.class);
        RestActionImpl restAction = mock(RestActionImpl.class);

        when(messageReceivedEvent.getMessage()).thenReturn(message);
        when(messageReceivedEvent.getAuthor()).thenReturn(user);
        when(messageReceivedEvent.getChannel()).thenReturn(textChannel);

        when(user.isBot()).thenReturn(false);
        when(textChannel.sendTyping()).thenReturn(restAction);
        when(textChannel.sendMessage((CharSequence) any())).thenReturn(messageAction);

        when(message.getContentRaw()).thenReturn("/test");
        doNothing().when(messageAction).queue();
        doNothing().when(restAction).queue();

        mainController.onGuildMessageReceived(messageReceivedEvent);
    }

    @Test
    public void testOnGuildMessageReceivedOnProject() throws Exception {
        GuildMessageReceivedEvent messageReceivedEvent = mock(GuildMessageReceivedEvent.class);
        Message message = mock(Message.class);
        User user = mock(User.class);

        when(messageReceivedEvent.getMessage()).thenReturn(message);
        when(messageReceivedEvent.getAuthor()).thenReturn(user);

        when(user.isBot()).thenReturn(false);
        when(message.getContentRaw()).thenReturn("/project");
        doNothing().when(projectController).onGuildMessageReceivedProject(messageReceivedEvent);

        mainController.onGuildMessageReceived(messageReceivedEvent);
        verify(projectController, times(1)).onGuildMessageReceivedProject(any());
    }

    @Test
    public void testOnGuildMessageReceivedOnHistory() throws Exception {
        GuildMessageReceivedEvent messageReceivedEvent = mock(GuildMessageReceivedEvent.class);
        Message message = mock(Message.class);
        User user = mock(User.class);

        when(messageReceivedEvent.getMessage()).thenReturn(message);
        when(messageReceivedEvent.getAuthor()).thenReturn(user);

        when(user.isBot()).thenReturn(false);
        when(message.getContentRaw()).thenReturn("/history");
        doNothing().when(historyController).onGuildMessageReceivedHistory(messageReceivedEvent);

        mainController.onGuildMessageReceived(messageReceivedEvent);
        verify(historyController, times(1)).onGuildMessageReceivedHistory(any());
    }

    @Test
    public void testOnPrivateMessageReceived() throws Exception {
        PrivateMessageReceivedEvent messageReceivedEvent = mock(PrivateMessageReceivedEvent.class);
        User user = mock(User.class);

        when(messageReceivedEvent.getAuthor()).thenReturn(user);
        when(user.isBot()).thenReturn(false);
        doNothing().when(reminderController).onPrivateMessageReceivedReminder(messageReceivedEvent);

        mainController.onPrivateMessageReceived(messageReceivedEvent);
        verify(reminderController, times(1)).onPrivateMessageReceivedReminder(any());
    }

    @Test
    public void testOnGuildMemberJoin() throws Exception {
        GuildMemberJoinEvent memberJoinEvent = mock(GuildMemberJoinEvent.class);
        Member member = mock(Member.class);

        when(memberJoinEvent.getMember()).thenReturn(member);
        when(member.getId()).thenReturn("ini id");

        mainController.onGuildMemberJoin(memberJoinEvent);
        verify(memberRepository, times(1)).addMember(any());
    }

}
