package com.b12.proman.project.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.b12.proman.project.core.Member;
import com.b12.proman.project.core.Task;
import com.b12.proman.project.repository.MemberRepository;
import com.b12.proman.project.repository.TaskRepository;
import com.b12.proman.reminder.core.Reminder;
import com.b12.proman.reminder.repository.ReminderRepository;
import java.util.HashMap;
import java.util.Map;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.requests.RestAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class MemberServiceTest {

    @InjectMocks
    MemberServiceImpl memberService;

    @Spy
    MemberRepository memberRepository;

    @Spy
    ReminderRepository reminderRepository;

    @Spy
    TaskRepository taskRepository;

    Member member;

    @BeforeEach
    public void setUp() {
        member = spy(new Member("ini id"));
    }

    @Test
    public void testMemberServiceIsLeaderTrue() throws Exception {
        when(memberRepository.getMemberById("ini id")).thenReturn(member);
        memberService.addProject("ini id", "the project");
        memberService.setLeader("ini id", "the project");

        assertTrue(memberService.isLeader("ini id", "the project"));
    }

    @Test
    public void testMemberServiceIsLeaderFalse() throws Exception {
        when(memberRepository.getMemberById("ini id")).thenReturn(member);
        memberService.addProject("ini id", "the project");

        assertFalse(memberService.isLeader("ini id", "the project"));
    }

    @Test
    public void testMemberServiceAddProject() throws Exception {
        when(memberRepository.getMemberById("ini id")).thenReturn(member);

        memberService.addProject("ini id", "the project");

        verify(member, times(1)).getProjects();
        verify(member, times(1)).setProjects(any());
    }

    @Test
    public void testMemberServiceDeleteProject() throws Exception {
        when(memberRepository.getMemberById("ini id")).thenReturn(member);

        memberService.addProject("ini id", "the project");
        memberService.deleteProject("ini id", "the project");

        verify(member, times(2)).getProjects();
        verify(member, times(2)).setProjects(any());
    }

    @Test
    public void testMemberServiceAddTask() throws Exception {
        Task task = spy(new Task("judul", "desc", "2020-01-01-01-01-01"));

        when(memberRepository.getMemberById("ini id")).thenReturn(member);
        when(taskRepository.getTask("the task")).thenReturn(task);

        memberService.addTask("ini id", "the task");

        verify(member, times(1)).getTasks();
        verify(member, times(1)).setTasks(any());
    }

    @Test
    public void testMemberCreateEmbedMessage() throws Exception {
        EmbedBuilder embedBuilderCreate = memberService.createEmbedMessage("the project", "create");
        EmbedBuilder embedBuilderEdit = memberService.createEmbedMessage("the project", "edit");
        EmbedBuilder embedBuilderDelete = memberService.createEmbedMessage("the project", "delete");

        assertEquals(embedBuilderCreate.getDescriptionBuilder().toString(), "Contact your leader for further information.");
        assertEquals(embedBuilderEdit.getDescriptionBuilder().toString(), "Contact your leader for further information.");
        assertEquals(embedBuilderDelete.getDescriptionBuilder().toString(), "Contact your leader for further information.");
    }

    @Test
    public void testMemberUpdateAsObserver() throws Exception {
        Map<String, Boolean> testProjects = new HashMap<>();
        testProjects.put("the project", false);
        User user = mock(User.class);
        RestAction<PrivateChannel> restAction = mock(RestAction.class);
        RestAction<Message> restAction1 = mock(RestAction.class);

        when(memberRepository.getMemberById("ini id")).thenReturn(member);
        when(member.getProjects()).thenReturn(testProjects);
        when(member.getUser()).thenReturn(user);
        doReturn(restAction).when(user).openPrivateChannel();
        doReturn(restAction1).when(restAction).flatMap(ArgumentMatchers.any());
        doNothing().when(restAction1).queue();

        memberService.update("ini id", "the project", "create");
        memberService.update("ini id", "the project", "edit");
        memberService.update("ini id", "the project", "delete");
    }


}
