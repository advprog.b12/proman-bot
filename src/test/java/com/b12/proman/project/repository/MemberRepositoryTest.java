package com.b12.proman.project.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.b12.proman.PromanApplication;
import com.b12.proman.project.core.Member;
import java.util.ArrayList;
import java.util.List;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;

public class MemberRepositoryTest {

    private MemberRepository memberRepository;
    private Member sampleMember;
    private List<Member> memberList;
    private JDA jda;

    @BeforeEach
    public void setUp() {
        memberRepository = new MemberRepositoryImpl();
        memberList = new ArrayList<>();
        sampleMember = new Member("688766230676766769");
        memberList.add(sampleMember);
        try {
            jda = JDABuilder
                .createDefault("ODM3NTYwOTU4Njc4MjA0NDI2.YIuVbQ.2CBnr7zygLX8pxwkr96NXN3Vql0")
                .build();
        } catch (Exception e) {
            System.out.println("Exception occurs");
        }
    }

    @Test
    public void testMemberRepoGetMemberByIdExistItShouldReturnMember() throws Exception {
        ReflectionTestUtils.setField(memberRepository, "memberList", memberList);
        Member getMember = memberRepository.getMemberById("688766230676766769");

        assertThat(getMember).isEqualTo(sampleMember);
    }

    @Test
    public void testMemberRepoGetMemberByIdNotExistItShouldAddAndReturnMember() throws Exception {
        ReflectionTestUtils.setField(memberRepository, "memberList", memberList);

        try (MockedStatic<PromanApplication> utilities = Mockito.mockStatic(PromanApplication.class)) {
            utilities.when(PromanApplication::getJDA).thenReturn(jda);

            assertEquals(memberList.size(), 1);
            Member getMember = memberRepository.getMemberById("688766230676766760");
            assertEquals(getMember.getId(), "688766230676766760");
            assertEquals(memberList.size(), 2);
        }
    }

}
