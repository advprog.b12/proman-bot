package com.b12.proman.project.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.b12.proman.reminder.core.Reminder;
import java.util.HashMap;
import java.util.Map;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class MemberTest {

    @InjectMocks
    Member member = new Member("ini id");

    @Mock
    User user;

    @Spy
    Project project;

    @Test
    public void testMemberGetId() throws Exception {
        assertEquals(member.getId(), "ini id");
    }

    @Test
    public void testMemberGetAndSetProject() throws Exception {
        Map<String, Boolean> testProjects = new HashMap<>();
        member.setProjects(testProjects);
        assertEquals(member.getProjects(), testProjects);
    }

    @Test
    public void testMemberGetAndSetTasks() throws Exception {
        Map<String, Reminder> testTasks = new HashMap<>();
        member.setTasks(testTasks);
        assertEquals(member.getTasks(), testTasks);
    }

    @Test
    public void testMemberGetAndSetUser() throws Exception {
        member.setUser(user);
        assertEquals(member.getUser(), user);
    }

    @Test
    public void testMemberNotifyReminder() throws Exception {
        User user = mock(User.class);
        Reminder reminder = spy(new Reminder(user));
        member.getTasks().put("ini task", reminder);
        doNothing().when(reminder).off();

        member.notifyReminder("ini task");
        verify(reminder, times(1)).off();
    }

}
