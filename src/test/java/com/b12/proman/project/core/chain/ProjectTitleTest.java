package com.b12.proman.project.core.chain;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.b12.proman.project.core.Project;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ProjectTitleTest {

    private Project project;

    @BeforeEach
    public void setUp() {
        project = new Project();
    }

    @Test
    void creating() {
        project.setTitle("Tes Judul Proyek");
        assertEquals("Tes Judul Proyek", project.getTitle());
    }
}