package com.b12.proman.project.core.chain;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.b12.proman.project.core.Project;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ProjectMemberTest {

    private Project project;

    @BeforeEach
    public void setUp() {
        project = new Project();
    }

    @Test
    void creating() {
        project.setMember("Tes Member Proyek");
        assertEquals("Tes Member Proyek", project.getMember());
    }
}