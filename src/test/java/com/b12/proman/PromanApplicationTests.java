package com.b12.proman;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class PromanApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    public void main() {
        PromanApplication.main(new String[]{});
    }

    @Test
    public void testGetJDA() {
        assertNotNull(PromanApplication.getJDA());
    }
}
