package com.b12.proman.reminder.repository;

import static org.assertj.core.api.Assertions.assertThat;

import com.b12.proman.reminder.core.Reminder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ReminderRepositoryTest {
    
    private ReminderRepository reminderRepo;
    private Reminder reminder;
    private Map<String, Reminder> remindersMap;

    @Mock
    User user;

    @BeforeEach
    public void setUp() {
        reminderRepo = new ReminderRepository();
        reminder = new Reminder("example", user);
        remindersMap = new HashMap<String, Reminder>();
        remindersMap.put("example", reminder);
    }

    @Test
    public void testReminderRepositoryOnAddingReminder() throws Exception {
        Reminder newReminder = new Reminder("Mei Mei", user);
        reminderRepo.add(newReminder);
        Reminder acquiredReminder = reminderRepo.findByTitle(newReminder.getTitle());

        assertThat(acquiredReminder).isEqualTo(newReminder);
    }


    @Test
    public void testReminderRepositoryFindByTitle() {
        Reminder reminderInstance = new Reminder(user);
        reminderRepo.add(reminderInstance);

        Reminder reminder = reminderRepo.findByTitle(reminderInstance.getTitle());

        assertThat(reminder).isNotNull();
        assertThat(reminder.getTitle()).isEqualTo(reminderInstance.getTitle());
        assertThat(reminder.getStrategy()).isEqualTo(reminderInstance.getStrategy());
    }

    @Test
    public void testReminderRepositoryOnFindingNonExistingReminderByAlias() {
        Reminder notFoundReminder = reminderRepo.findByTitle("Knight");

        assertThat(notFoundReminder).isNull();
    }

    @Test
    public void testReminderRepositoryOnFindingAllReminders() {
        reminderRepo.add(new Reminder(user));
        reminderRepo.add(new Reminder(user));
        reminderRepo.add(new Reminder(user));

        List<Reminder> allReminders = (List<Reminder>) reminderRepo.findAll();

        assertThat(allReminders.size()).isEqualTo(3);
    }
    
}
