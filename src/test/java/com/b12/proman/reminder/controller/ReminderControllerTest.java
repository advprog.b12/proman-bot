package com.b12.proman.reminder.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.b12.proman.reminder.service.ReminderService;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent;
import net.dv8tion.jda.api.requests.RestAction;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ReminderControllerTest {
    // TODO: masi failed
    
    /*
    @InjectMocks
    ReminderController reminderController;

    @Spy
    ReminderService reminderService;

    @Mock
    Message message;

    @Mock
    PrivateChannel privateChannel;

    @Mock
    MessageAction messageAction;

    @Mock
    RestAction restAction;

    @Test
    void testOnPrivateMessageReceivedOnChangeReminder() throws Exception {
        PrivateMessageReceivedEvent privateMessageReceivedEvent = mock(PrivateMessageReceivedEvent.class);

        when(privateMessageReceivedEvent.getMessage()).thenReturn(message);
        when(privateMessageReceivedEvent.getChannel()).thenReturn(privateChannel);

        when(privateChannel.sendTyping()).thenReturn(restAction);
        when(privateChannel.sendMessage((CharSequence) any())).thenReturn(messageAction);

        when(message.getContentRaw()).thenReturn("/change-reminder");
        doNothing().when(messageAction).queue();
        doNothing().when(restAction).queue();

        reminderController.onPrivateMessageReceivedReminder(privateMessageReceivedEvent);
        verify(reminderService, times(1)).setReminder(anyString(), anyString(), anyString());

    }

    @Test
    void testOnPrivateMessageReceivedOnSetOn() throws Exception {
        PrivateMessageReceivedEvent privateMessageReceivedEvent = mock(PrivateMessageReceivedEvent.class);

        when(privateMessageReceivedEvent.getMessage()).thenReturn(message);
        when(privateMessageReceivedEvent.getChannel()).thenReturn(privateChannel);

        when(privateChannel.sendTyping()).thenReturn(restAction);
        when(privateChannel.sendMessage((CharSequence) any())).thenReturn(messageAction);

        when(message.getContentRaw()).thenReturn("/set-on");
        doNothing().when(messageAction).queue();
        doNothing().when(restAction).queue();

        reminderController.onPrivateMessageReceivedReminder(privateMessageReceivedEvent);
        verify(reminderService, times(1)).setOn(anyString());

    }

    @Test
    void testOnPrivateMessageReceivedOnSetOff() throws Exception {
        PrivateMessageReceivedEvent privateMessageReceivedEvent = mock(PrivateMessageReceivedEvent.class);

        when(privateMessageReceivedEvent.getMessage()).thenReturn(message);
        when(privateMessageReceivedEvent.getChannel()).thenReturn(privateChannel);

        when(privateChannel.sendTyping()).thenReturn(restAction);
        when(privateChannel.sendMessage((CharSequence) any())).thenReturn(messageAction);

        when(message.getContentRaw()).thenReturn("/set-off");
        doNothing().when(messageAction).queue();
        doNothing().when(restAction).queue();

        reminderController.onPrivateMessageReceivedReminder(privateMessageReceivedEvent);
        verify(reminderService, times(1)).setOff(anyString());

    }
    */
}
