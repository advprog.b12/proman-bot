package com.b12.proman.reminder.service.changer;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.b12.proman.reminder.core.Reminder;
import com.b12.proman.reminder.core.strategy.CustomReminder;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CustomChangerTest {
    
    @InjectMocks
    CustomChanger customChanger;

    @Test
    void testChangeStrategyWithArgs() throws Exception {
        User user = mock(User.class);
        Reminder reminder = mock(new Reminder("Example", user).getClass());

        String[] args = {"3h", "3"};
        customChanger.changeStrategy(reminder, args);
        verify(reminder).setReminder(any(CustomReminder.class));
    }

    @Test
    void testChangeStrategyIfNoArgs() throws Exception {
        User user = mock(User.class);
        Reminder reminder = mock(new Reminder("Example", user).getClass());

        String[] args = {"2d"};
        customChanger.changeStrategy(reminder, args);
        verify(reminder).setReminder(any(CustomReminder.class));
    }
}
