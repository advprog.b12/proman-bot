package com.b12.proman.reminder.service.changer;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.b12.proman.reminder.core.Reminder;
import com.b12.proman.reminder.core.strategy.DailyReminder;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class DailyChangerTest {

    @InjectMocks
    DailyChanger dailyChanger;

    @Test
    void testChangeStrategyIfNoArgs() throws Exception {
        User user = mock(User.class);
        Reminder reminder = mock(new Reminder("Example", user).getClass());

        String[] args = {"1"};
        dailyChanger.changeStrategy(reminder, args);
        verify(reminder).setReminder(any(DailyReminder.class));
    }

    @Test
    void testChangeStrategyWithArgs() throws Exception {
        User user = mock(User.class);
        Reminder reminder = mock(new Reminder("Example", user).getClass());

        String[] args = {};
        dailyChanger.changeStrategy(reminder, args);
        verify(reminder).setReminder(any(DailyReminder.class));
    }
}
