package com.b12.proman.reminder.service.changer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.b12.proman.reminder.core.Reminder;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

@ExtendWith(MockitoExtension.class)
public class ReminderChangerTest {
    
    @InjectMocks
    ReminderChanger reminderChanger;

    @Mock
    ReminderChanger attributeChanger;

    @Mock
    Reminder reminder;

    @Mock
    User user;

    @Test
    void testChangeStrategyWorksProperly() throws Exception {
        reminder = new Reminder("example", user);
        reminderChanger.changeStrategy(reminder, "1");
        verify(attributeChanger).changeStrategy(reminder, "1");
    }

    @Test
    void testCoordinateWorks() throws Exception {
        reminderChanger.coordinate("-h");
        assertTrue(reminderChanger.getChanger() instanceof HourlyChanger);

        reminderChanger.coordinate("-d");
        assertTrue(reminderChanger.getChanger() instanceof DailyChanger);

        reminderChanger.coordinate("-w");
        assertTrue(reminderChanger.getChanger() instanceof WeeklyChanger);

        reminderChanger.coordinate("-m");
        assertTrue(reminderChanger.getChanger() instanceof MonthlyChanger);

        reminderChanger.coordinate("-c");
        assertTrue(reminderChanger.getChanger() instanceof CustomChanger);
    }
}
