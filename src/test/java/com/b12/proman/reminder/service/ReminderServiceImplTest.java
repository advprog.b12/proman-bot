package com.b12.proman.reminder.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.b12.proman.reminder.core.Reminder;
import com.b12.proman.reminder.repository.ReminderRepository;
import com.b12.proman.reminder.service.changer.ReminderChanger;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ReminderServiceImplTest {
    
    @InjectMocks
    ReminderServiceImpl reminderServiceImpl;

    @Mock
    ReminderRepository reminderRepository;

    @Mock
    ReminderChanger reminderChanger;

    @Test
    void testSetReminderWorksProperly() throws Exception {
        User user = mock(User.class);
        Reminder reminder = mock(new Reminder("example", user).getClass());
        reminderRepository.add(reminder);
        when(reminderRepository.findByTitle("example")).thenReturn(reminder);

        // For case "-h"
        String answer = reminderServiceImpl.setReminder("example", "-h", anyString());
        verify(reminderChanger, times(1)).coordinate("-h");
        assertThat(answer).isEqualTo("✅ Succesfully change your reminder!!%n%s", reminder.getInfo());

        // For case "-d"
        answer = reminderServiceImpl.setReminder("example", "-d", anyString());
        verify(reminderChanger, times(1)).coordinate("-d");
        assertThat(answer).isEqualTo("✅ Succesfully change your reminder!!%n%s", reminder.getInfo());

        // For case "-w"
        answer = reminderServiceImpl.setReminder("example", "-w", anyString());
        verify(reminderChanger, times(1)).coordinate("-w");
        assertThat(answer).isEqualTo("✅ Succesfully change your reminder!!%n%s", reminder.getInfo());

        // For case "-m"
        answer = reminderServiceImpl.setReminder("example", "-m", anyString());
        verify(reminderChanger, times(1)).coordinate("-m");
        assertThat(answer).isEqualTo("✅ Succesfully change your reminder!!%n%s", reminder.getInfo());

        // For case "-c"
        answer = reminderServiceImpl.setReminder("example", "-c", anyString());
        verify(reminderChanger, times(1)).coordinate("-c");
        assertThat(answer).isEqualTo("✅ Succesfully change your reminder!!%n%s", reminder.getInfo());

        // For default case
        answer = reminderServiceImpl.setReminder("example", "", anyString());
        assertThat(answer).isEqualTo("❌ Your argument is invalid!");

        verify(reminderRepository, times(6)).findByTitle("example");
        verify(reminderChanger, times(5)).changeStrategy(eq(reminder), anyString());
    }

    @Test
    void testSetOnWorksProperly() throws Exception {
        User user = mock(User.class);
        Reminder reminder = mock(new Reminder("example", user).getClass());
        reminderRepository.add(reminder);
        when(reminderRepository.findByTitle("example")).thenReturn(reminder);

        doReturn(false).when(reminder).isOn();   
        String answer = reminderServiceImpl.setOn("example");
        assertThat(answer).isEqualTo("example is switched on");
   
        doReturn(true).when(reminder).isOn();
        answer = reminderServiceImpl.setOn("example");
        assertThat(answer).isEqualTo("example is already on");
    }

    @Test
    void testSetOffWorksProperly() throws Exception {
        User user = mock(User.class);
        Reminder reminder = mock(new Reminder("example", user).getClass());
        reminderRepository.add(reminder);
        when(reminderRepository.findByTitle("example")).thenReturn(reminder);

        doReturn(false).when(reminder).isOn();
        String answer = reminderServiceImpl.setOff("example");
        assertThat(answer).isEqualTo("example is already off");

        doReturn(true).when(reminder).isOn();
        answer = reminderServiceImpl.setOff("example");
        assertThat(answer).isEqualTo("example is switched off");
    }

    @Test
    void testAddToRepo() throws Exception {
        User user = mock(User.class);
        Reminder reminder = mock(new Reminder("example", user).getClass());
        reminderRepository.add(reminder);
        assertEquals(eq(reminder), reminderRepository.findByTitle("example"));
    }
}
