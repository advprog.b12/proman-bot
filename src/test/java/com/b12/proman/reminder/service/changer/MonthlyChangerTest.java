package com.b12.proman.reminder.service.changer;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.b12.proman.reminder.core.Reminder;
import com.b12.proman.reminder.core.strategy.MonthlyReminder;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class MonthlyChangerTest {

    @InjectMocks
    MonthlyChanger monthlyChanger;
    
    @Test
    void testChangeStrategyWithArgs() throws Exception {
        User user = mock(User.class);
        Reminder reminder = mock(new Reminder("Example", user).getClass());

        String[] args = {"3"};
        monthlyChanger.changeStrategy(reminder, args);
        verify(reminder).setReminder(any(MonthlyReminder.class));
    }

    @Test
    void testChangeStrategyIfNoArgs() throws Exception {
        User user = mock(User.class);
        Reminder reminder = mock(new Reminder("Example", user).getClass());

        String[] args = {};
        monthlyChanger.changeStrategy(reminder, args);
        verify(reminder).setReminder(any(MonthlyReminder.class));
    }
}
