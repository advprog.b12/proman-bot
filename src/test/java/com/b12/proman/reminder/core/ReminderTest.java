// package com.b12.proman.reminder.core;

// import java.lang.reflect.Method;
// import java.lang.reflect.Modifier;

// import com.b12.proman.reminder.core.strategy.ReminderStrategy;
// import static org.junit.jupiter.api.Assertions.*;
// import static org.mockito.Mockito.verify;

// import org.junit.jupiter.api.BeforeEach;
// import org.junit.jupiter.api.Test;
// import org.junit.jupiter.api.extension.ExtendWith;
// import org.mockito.InjectMocks;
// import org.mockito.Mock;
// import org.mockito.MockitoAnnotations;
// import org.mockito.junit.jupiter.MockitoExtension;

// import static org.assertj.core.api.Assertions.assertThat;

// import net.dv8tion.jda.api.entities.User;

// @ExtendWith(MockitoExtension.class)
// public class ReminderTest {

//     private Class<?> reminderClass;

//     @InjectMocks
//     private Reminder reminder;

//     @Mock
//     ReminderStrategy reminderStrategy;

//     @Mock
//     User user;

//     @BeforeEach
//     public void setUp() throws Exception {
//         MockitoAnnotations.openMocks(this);
//         reminderClass = Class.forName("com.b12.proman.reminder.core.Reminder");
//         reminder = new Reminder("Dummy", user);
//         reminder.setReminder(reminderStrategy);
//     }

//     @Test
//     public void testReminderIsAPublicClass() throws Exception {
//         assertTrue(Modifier.isPublic(reminderClass.getModifiers()));
//     }

//     @Test
//     public void testDefaultReminderIsDaily() throws Exception {
//         Reminder newReminder = new Reminder("example", user);
//         assertEquals(
//             Class.forName("com.b12.proman.reminder.core.strategy.DailyReminder"), 
//             newReminder.getStrategy().getClass());
//     }

//     @Test
//     public void testGetTitleWorks() throws Exception {
//         assertEquals("Dummy", reminder.getTitle());
//     }

//     @Test
//     public void testSetTitleWorks() throws Exception {
//         reminder.setTitle("reminder");
//         assertEquals("reminder", reminder.getTitle());
//     }

//     @Test
//     public void testHasReminderStrategySetter() throws Exception {
//         Method setReminder = reminderClass.getDeclaredMethod("setReminder", ReminderStrategy.class);

//         assertTrue(Modifier.isPublic(setReminder.getModifiers()));
//         assertEquals(1, setReminder.getParameterCount());
//         assertEquals("void", setReminder.getGenericReturnType().getTypeName());
//     }

//     @Test
//     void testGetterAndSetterForStrategyWorks() throws Exception {
//         reminder.setReminder(reminderStrategy);
//         assertEquals(reminderStrategy, reminder.getStrategy());
//     }

//     @Test
//     void testReminderHasMethodOn() throws Exception {
//         Method on = reminderClass.getDeclaredMethod("on");

//         assertTrue(Modifier.isPublic(on.getModifiers()));
//         assertEquals(0, on.getParameterCount());
//         assertEquals("void", on.getGenericReturnType().getTypeName());
//     }

//     // @Test
//     // void testOnAndOff() throws Exception {
//     //     reminder.off();
//     //     System.out.println(reminder.isOn());
//     //     assertFalse(reminder.isOn());
//     //     verify(reminderStrategy).stop();

//     //     reminder.on();
//     //     System.out.println(reminder.isOn());
//     //     assertTrue(reminder.isOn());
//     //     verify(reminderStrategy).remind();
//     // }

//     @Test
//     void testReminderHasMethodOff() throws Exception {
//         Method off = reminderClass.getDeclaredMethod("off");

//         assertTrue(Modifier.isPublic(off.getModifiers()));
//         assertEquals(0, off.getParameterCount());
//         assertEquals("void", off.getGenericReturnType().getTypeName());
//     }

//     @Test
//     void testReminderHasMethodGetInfoAndWorksProperly() throws Exception {
//         Method getInfo = reminderClass.getDeclaredMethod("getInfo");

//         assertTrue(Modifier.isPublic(getInfo.getModifiers()));
//         assertEquals(0, getInfo.getParameterCount());
//         assertEquals("java.lang.String", getInfo.getGenericReturnType().getTypeName());

//         String answer = reminder.getInfo();
//         assertThat(answer).isEqualTo("Title: %s %n %s", reminder.getTitle(), reminderStrategy.getInfo());
//     }
// }
