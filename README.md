[![pipeline status](https://gitlab.com/advprog.b12/proman-bot/badges/master/pipeline.svg)](https://gitlab.com/advprog.b12/proman-bot/-/commits/master)
[![coverage report](https://gitlab.com/advprog.b12/proman-bot/badges/master/coverage.svg)](https://gitlab.com/advprog.b12/proman-bot/-/commits/master)

## TK Advanced Programming Kelompok B12

##### Coverage Task

[![coverage report](https://gitlab.com/michaelsusanto81/advanced-programming-tutorial/badges/tutorial-5/coverage.svg)](https://gitlab.com/advprog.b12/proman-bot/-/commits/task)

##### Coverage Reminder
[![coverage report](https://gitlab.com/advprog.b12/proman-bot/badges/reminder2/coverage.svg)](https://gitlab.com/advprog.b12/proman-bot/-/commits/reminder2)

##### Coverage Member
[![coverage report](https://gitlab.com/advprog.b12/proman-bot/badges/member/coverage.svg)](https://gitlab.com/advprog.b12/proman-bot/-/commits/member)


| No  |       Nama        |    NPM     |    Fitur     |
| :-: | :---------------: | :--------: | :----------: |
|  1  |    Ihwan Edi S    | 1706074934 |   Project    |
|  2  |  Primo Giancarlo  | 1806205180 |   History    |
|  3  |   Fadli Aulawi    | 1906285623 |    Member    |
|  4  | Bintang Dominique | 1906292963 |     Task     |
|  5  |    Alif Iqbal     | 1906398282 |   Reminder   |

## Discord Bot Project Manager (Proman)

Sekarang ini, Discord menjadi media sosial yang populer di dunia. Media sosial yang awalnya ditujukan untuk komunitas game ini sekarang sudah banyak digunakan untuk hal lain, contohnya di bidang pendidikan. Discord juga ramah akan bot, banyak sekali bot yang dibuat oleh orang-orang bahkan bisa kita sendiri dan nantinya akan membantu kita dalam menggunakan Discord. Meski begitu, belum banyak bot Discord yang bersifat produktif dalam artian produktif dalam pekerjaann.

ProMan Bot hadir sebagai salah satu wadah berupa bot Discord untuk menunjang produktivitas dalam tim. Bot ini akan mampu untuk melakukan sejumlah tugas secara otomatis seperti mengelola proyek dalam kelompok, melakukan progress tracking hingga mengingatkan kita terkait tugas yang sedang diberikan. Dengan bot ini, kita bisa tetap bersantai di Discord sebagaimana biasa, tetapi tidak lupa dengan proyek atau tugas yang sedang kita kerjakan.
